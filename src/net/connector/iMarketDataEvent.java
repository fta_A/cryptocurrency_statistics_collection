package net.connector;

public interface iMarketDataEvent {
	void MarketDataChanged(int connectionId, MarketData marketData);
}
