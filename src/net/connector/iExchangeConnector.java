package net.connector;

public interface iExchangeConnector {
	String getConnectionString();	
	void setConnectionString(String connectionString);

	String getExchangeCode();
	void setExchangeCode(String exchangeCode);
	
	
	void Connect();
	void Disconnect();	
	
	int ConnectionId();
	
	void AddConnectionEventListener(iConnectionEvent listener);
	void AddMarketDataListener(iMarketDataEvent listener);
	void AddOrderStateListener(iOrderStateEvent listener);
	void AddOrder(Order order);
	void KillOrder(Order order);
}
