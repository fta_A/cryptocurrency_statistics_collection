package net.connector;

import java.util.ArrayList;

import net.codejava.Quote;

public class Order {
	public int userref;
	public String trdPair;
	public OrderDirection dir;
	public MarketType marketType;
	public double price;
	public double amount;
	public OrderState orderState;
	public OrderType orderType;
	public String errorState;
	public long sentUnixTime;
	
	public ArrayList<String> transIdList = new ArrayList<String>();
	public iExchangeConnector exchangeConnector;
	public Quote quote;
	
	public Order getCopy() {
		Order order = new Order();
		order.amount = this.amount;
		order.dir = this.dir;
		order.marketType = this.marketType;
		order.orderState = this.orderState;
		order.price = this.price;
		order.trdPair = this.trdPair;
		order.userref = this.userref;
		order.exchangeConnector = this.exchangeConnector;
		order.quote = this.quote;
		order.sentUnixTime = this.sentUnixTime;
		
		
		return order;
	}
	
	public enum OrderDirection{
		buy,
		sell
	}
	
	public enum MarketType{
		limit,
		market
	}
	
	public enum OrderType{
		NewOrder,
		MoveOrder,
		KillOrder
	}
	
	public enum OrderState{
		Adding,
		Moving,
		Killing,
		
		Active,
		Killed,
		Executed,
		
		Refused
	}
}
