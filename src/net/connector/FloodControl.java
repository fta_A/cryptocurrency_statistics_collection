package net.connector;

public class FloodControl {
	public int floodPeriodSec;
	public int maxFloodRequests;
	
	
	
	long[] data;
	int first;
	int data_size;
	
	int DATA_CAPACITY = 100000;
	
	public FloodControl() {
		this.floodPeriodSec = 0;
		this.maxFloodRequests = 0;
		
		this.data = new long[DATA_CAPACITY];
		this.first = 0;
		this.data_size = 0;
		
	}
	
	public FloodControl(int floodPeriodSec, int maxFloodRequests){
		this.floodPeriodSec = floodPeriodSec;
		this.maxFloodRequests = maxFloodRequests;
		
		this.data = new long[DATA_CAPACITY];
		this.first = 0;
		this.data_size = 0;
	}
	
	synchronized public int AvailableRequests(){
		long currNanoTime = System.currentTimeMillis();//System.nanoTime();

		while(data_size != 0 && ((currNanoTime - data[first]) >= this.floodPeriodSec * 1000)){
			first = (first + 1) % DATA_CAPACITY;
			data_size--;
		}
		
		return maxFloodRequests - data_size;		
	}
	
	synchronized public void NewRequest(){
		data[(first + data_size) % DATA_CAPACITY] = System.currentTimeMillis();//System.nanoTime();
		data_size++;
	}
}
