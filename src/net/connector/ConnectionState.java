package net.connector;

public class ConnectionState {
	public String stateDescription;
	public ConnectionStateTypesEnum connectionStateType;
	
	public enum ConnectionStateTypesEnum {
		Connected,
		Disconnected,
		Pending
	}
}
