package net.connector;

public class MarketData {
	private String trdPair;
	private int id;
	private float last;
	private double lowestAsk;
	private double highestBid;
	private float percentChange;
	private float baseVolume;
	private double quoteVolume;
	private int isFrozen;
	private float high24hr;
	private float low24hr;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public float getLast() {
		return last;
	}
	public void setLast(float last) {
		this.last = last;
	}
	public double getLowestAsk() {
		return lowestAsk;
	}
	public void setLowestAsk(double lowestAsk) {
		this.lowestAsk = lowestAsk;
	}
	public double getHighestBid() {
		return highestBid;
	}
	public void setHighestBid(double highestBid) {
		this.highestBid = highestBid;
	}
	public float getPercentChange() {
		return percentChange;
	}
	public void setPercentChange(float percentChange) {
		this.percentChange = percentChange;
	}
	public float getBaseVolume() {
		return baseVolume;
	}
	public void setBaseVolume(float baseVolume) {
		this.baseVolume = baseVolume;
	}
	public double getQuoteVolume() {
		return quoteVolume;
	}
	public void setQuoteVolume(double quoteVolume) {
		this.quoteVolume = quoteVolume;
	}
	public int getIsFrozen() {
		return isFrozen;
	}
	public void setIsFrozen(int isFrozen) {
		this.isFrozen = isFrozen;
	}
	public float getHigh24hr() {
		return high24hr;
	}
	public void setHigh24hr(float high24hr) {
		this.high24hr = high24hr;
	}
	public float getLow24hr() {
		return low24hr;
	}
	public void setLow24hr(float low24hr) {
		this.low24hr = low24hr;
	}
	public String getTrdPair() {
		return trdPair;
	}
	public void setTrdPair(String trdPair) {
		this.trdPair = trdPair;
	}	
	
	@Override
	public String toString(){
		String result = "Pair: " + getTrdPair() + " ";
		//result += "Id: " + getId() + " ";
		//result += "Last: " + getLast() + "\n";
		result += "Lowest Ask: " + getLowestAsk() + " ";
		result += "Highest Bid: " + getHighestBid() + " ";
		//result += "Percent Change: " + getPercentChange() + "\n";
		//result += "Base Volume: " + getBaseVolume() + "\n";
		//result += "Quote Volume: " + getQuoteVolume() + "\n";
		//result += "Frozen: " + getIsFrozen() + " ";
		//result += "High 24h: " + getHigh24hr() + "\n";
		//result += "Low 24h: " + getLow24hr() + "\n";
		return result;
	}
}
