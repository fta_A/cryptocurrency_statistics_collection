package net.coinone;

import java.util.ArrayList;

public class CoinoneTicker {
	private String result;
	private String errorCode;
	ArrayList<CoinonePriceAmountPair> ask;
	ArrayList<CoinonePriceAmountPair> bid;
	private Long timestamp;
	private String currency;
	
	
	
	public String getResult() {
		return result;
	}



	public void setResult(String result) {
		this.result = result;
	}



	public String getErrorCode() {
		return errorCode;
	}



	public void setErrorCode(String errorCode) {
		this.errorCode = errorCode;
	}



	public ArrayList<CoinonePriceAmountPair> getAsk() {
		return ask;
	}



	public void setAsk(ArrayList<CoinonePriceAmountPair> ask) {
		this.ask = ask;
	}



	public ArrayList<CoinonePriceAmountPair> getBid() {
		return bid;
	}



	public void setBid(ArrayList<CoinonePriceAmountPair> bid) {
		this.bid = bid;
	}



	public Long getTimestamp() {
		return timestamp;
	}



	public void setTimestamp(Long timestamp) {
		this.timestamp = timestamp;
	}



	public String getCurrency() {
		return currency;
	}



	public void setCurrency(String currency) {
		this.currency = currency;
	}



	
}
