package net.korbit;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import javax.net.ssl.HttpsURLConnection;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import net.connector.FloodControl;
import net.connector.MarketData;
import net.connector.iMarketDataEvent;


public class MarketDataListener implements Runnable {
	iMarketDataEvent iMarketDataEvent;	
	FloodControl floodControl;
	int connectionId;
	
	ArrayList<String> tickersURLList = new ArrayList<String>();
	ArrayList<String> tickersList = new ArrayList<String>();
	HashMap<String, MarketData> marketDataHashMap = new HashMap<String, MarketData>();

	public MarketDataListener(iMarketDataEvent iMarketDataEvent, FloodControl floodControl, int connectionId){
		this.iMarketDataEvent = iMarketDataEvent;
		this.floodControl = floodControl;
		this.connectionId = connectionId;
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		
		tickersURLList.add("https://api.korbit.co.kr/v1/ticker/detailed?currency_pair=btc_krw");
		tickersURLList.add("https://api.korbit.co.kr/v1/ticker/detailed?currency_pair=etc_krw");
		tickersURLList.add("https://api.korbit.co.kr/v1/ticker/detailed?currency_pair=eth_krw");
		tickersURLList.add("https://api.korbit.co.kr/v1/ticker/detailed?currency_pair=xrp_krw");
		
		tickersList.add("btc_krw");
		tickersList.add("etc_krw");
		tickersList.add("eth_krw");
		tickersList.add("xrp_krw");
		
		int tickersURLListPosition = 0;
		
		ArrayList<URL> urlList = new ArrayList<URL>();		
		ObjectMapper mapper = new ObjectMapper();
			
		try {
			for (int it = 0; it < tickersURLList.size(); it++) {
				urlList.add(new URL(tickersURLList.get(it)));		
				
				String ticker = tickersList.get(it);
				MarketData marketData = new MarketData();
				marketData.setTrdPair(ticker);
				marketDataHashMap.put(ticker, marketData);

			}
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		Random rnd = new Random();
		
		while(true){
			
			long mills = Math.abs(rnd.nextLong()) % 100; 
			
			try {
				Thread.sleep(mills);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			synchronized (this.floodControl) {
				int availableRequests = this.floodControl.AvailableRequests();
				
				if(availableRequests > 0){
					String jsonString = null;
					
					try {
						URL url = urlList.get(tickersURLListPosition);
						
						HttpsURLConnection httpsURLConnection = (HttpsURLConnection)url.openConnection();
						httpsURLConnection.setRequestProperty("User-Agent", "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10.4; en-US; rv:1.9.2.2) Gecko/20100316 Firefox/3.6.2");
						//httpsURLConnection.connect();
						
						BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpsURLConnection.getInputStream()));
						jsonString = bufferedReader.readLine();
						
						JsonNode jn = mapper.readTree(jsonString);
						KorbitTicker ticker = mapper.convertValue(jn, KorbitTicker.class);
						
						MarketData marketData = marketDataHashMap.get(tickersList.get(tickersURLListPosition));
						if(marketData.getHighestBid() != ticker.getBid() || marketData.getLowestAsk() != ticker.getAsk()) {
							marketData.setLowestAsk(ticker.getAsk());
							marketData.setHighestBid(ticker.getBid());

							this.iMarketDataEvent.MarketDataChanged(this.connectionId, marketData);
						}
						
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					tickersURLListPosition = (tickersURLListPosition + 1)% tickersURLList.size();
					this.floodControl.NewRequest();
				}
			}
		}
	}



}

