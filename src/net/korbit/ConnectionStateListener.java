package net.korbit;

import java.time.LocalDateTime;
import java.util.Random;
import net.connector.ConnectionState;
import net.connector.ConnectionState.ConnectionStateTypesEnum;
import net.connector.FloodControl;
import net.connector.iConnectionEvent;

public class ConnectionStateListener implements Runnable {
	iConnectionEvent iConnectionEvent;	
	FloodControl floodControl;
	int connectionId;
	
	
	public ConnectionStateListener(iConnectionEvent iConnectionEvent, FloodControl floodControl, int connectionId){
		this.iConnectionEvent = iConnectionEvent;
		this.floodControl = floodControl;
		this.connectionId = connectionId;
	}
	
	@Override
	public void run() {
		Random rnd = new Random();
		
		// TODO Auto-generated method stub
		ConnectionState connectionState = new ConnectionState();
		
		while(true){
			
			long mills = Math.abs(rnd.nextLong()) % 100; 
			
			try {
				Thread.sleep(mills);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
			
			synchronized (this.floodControl) {
				int availableRequests = this.floodControl.AvailableRequests();
				
				if(availableRequests > 0){				
					int rndInt = Math.abs(rnd.nextInt()) % 10;
					if(rndInt < 5){
						connectionState.connectionStateType = ConnectionStateTypesEnum.Connected;
						connectionState.stateDescription = "Randomly connected: " + LocalDateTime.now();
					}
					else{
						connectionState.connectionStateType = ConnectionStateTypesEnum.Disconnected;
						connectionState.stateDescription = "Randomly disconnected: " + LocalDateTime.now();
					}
	
					this.iConnectionEvent.ConnectionStateChanged(connectionState);
					
					this.floodControl.NewRequest();
				}
				
			}
			
			
		}
		
		
	}

}
