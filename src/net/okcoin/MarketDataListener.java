package net.okcoin;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import javax.net.ssl.HttpsURLConnection;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import net.connector.FloodControl;
import net.connector.MarketData;
import net.connector.iMarketDataEvent;


public class MarketDataListener implements Runnable {
	iMarketDataEvent iMarketDataEvent;	
	FloodControl floodControl;
	int connectionId;
	ArrayList<String> tickersURLList = new ArrayList<String>();
	ArrayList<String> tickersList = new ArrayList<String>();
	HashMap<String, MarketData> marketDataHashMap = new HashMap<String, MarketData>();
	
	public MarketDataListener(iMarketDataEvent iMarketDataEvent, FloodControl floodControl, int connectionId){
		this.iMarketDataEvent = iMarketDataEvent;
		this.floodControl = floodControl;
		this.connectionId = connectionId;
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		
		tickersURLList.add("https://www.okcoin.com/api/v1/ticker.do?symbol=btc_usd");
		tickersURLList.add("https://www.okcoin.com/api/v1/ticker.do?symbol=ltc_usd");
		tickersURLList.add("https://www.okcoin.com/api/v1/ticker.do?symbol=eth_usd");
		
		tickersList.add("btc_usd");
		tickersList.add("ltc_usd");
		tickersList.add("eth_usd");
		
		
		int tickersURLListPosition = 0;
		
		ArrayList<URL> urlList = new ArrayList<URL>();		
		ObjectMapper mapper = new ObjectMapper();
		
		
		try {
			for (int it = 0; it < tickersURLList.size(); it++) {
				urlList.add(new URL(tickersURLList.get(it)));	
				
				String ticker = tickersList.get(it);
				MarketData marketData = new MarketData();
				marketData.setTrdPair(ticker);
				marketDataHashMap.put(ticker, marketData);

			}
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		Random rnd = new Random();
		
		
		while(true){
			
			long mills = Math.abs(rnd.nextLong()) % 100; 
			
			try {
				Thread.sleep(mills);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
			
			synchronized (this.floodControl) {
				int availableRequests = this.floodControl.AvailableRequests();
				
				if(availableRequests > 0){
					String jsonString = null;
					
					
					try {
						URL url = urlList.get(tickersURLListPosition);
						
						HttpsURLConnection httpsURLConnection = (HttpsURLConnection)url.openConnection();
						
						BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpsURLConnection.getInputStream()));
						jsonString = bufferedReader.readLine();
						
						JsonNode jn = mapper.readTree(jsonString);
						OkcoinTicker ticker = mapper.convertValue(jn.get("ticker"), OkcoinTicker.class);
							
						MarketData marketData = marketDataHashMap.get(tickersList.get(tickersURLListPosition));
						if(marketData.getHighestBid() != ticker.getBuy() || marketData.getLowestAsk() != ticker.getSell()) {
							marketData.setLowestAsk(ticker.getSell());
							marketData.setHighestBid(ticker.getBuy());

							this.iMarketDataEvent.MarketDataChanged(this.connectionId, marketData);
						}
						
						
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					tickersURLListPosition = (tickersURLListPosition + 1)% tickersURLList.size();
					this.floodControl.NewRequest();
				}
			}
		}
	}



}

