package net.bitflyer;

public class BitflyerTicker {
	
	
	private String product_code;
	private String timestamp;
	private int tick_id;
	private double best_bid;
	private double best_ask;
	private double best_bid_size;
	private double best_ask_size;
	private double total_bid_depth;
	private double total_ask_depth;
	private double ltp;
	private double volume;
	private double volume_by_product;
	
	
	public String getProduct_code() {
		return product_code;
	}
	public void setProduct_code(String product_code) {
		this.product_code = product_code;
	}
	public String getTimestamp() {
		return timestamp;
	}
	public void setTimestamp(String timestamp) {
		this.timestamp = timestamp;
	}
	public int getTick_id() {
		return tick_id;
	}
	public void setTick_id(int tick_id) {
		this.tick_id = tick_id;
	}
	public double getBest_bid() {
		return best_bid;
	}
	public void setBest_bid(double best_bid) {
		this.best_bid = best_bid;
	}
	public double getBest_ask() {
		return best_ask;
	}
	public void setBest_ask(double best_ask) {
		this.best_ask = best_ask;
	}
	public double getBest_bid_size() {
		return best_bid_size;
	}
	public void setBest_bid_size(double best_bid_size) {
		this.best_bid_size = best_bid_size;
	}
	public double getBest_ask_size() {
		return best_ask_size;
	}
	public void setBest_ask_size(double best_ask_size) {
		this.best_ask_size = best_ask_size;
	}
	public double getTotal_bid_depth() {
		return total_bid_depth;
	}
	public void setTotal_bid_depth(double total_bid_depth) {
		this.total_bid_depth = total_bid_depth;
	}
	public double getTotal_ask_depth() {
		return total_ask_depth;
	}
	public void setTotal_ask_depth(double total_ask_depth) {
		this.total_ask_depth = total_ask_depth;
	}
	public double getLtp() {
		return ltp;
	}
	public void setLtp(double ltp) {
		this.ltp = ltp;
	}
	public double getVolume() {
		return volume;
	}
	public void setVolume(double volume) {
		this.volume = volume;
	}
	public double getVolume_by_product() {
		return volume_by_product;
	}
	public void setVolume_by_product(double volume_by_product) {
		this.volume_by_product = volume_by_product;
	}

}
