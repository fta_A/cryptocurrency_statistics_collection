package net.bitflyer;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import javax.net.ssl.HttpsURLConnection;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import net.connector.FloodControl;
import net.connector.MarketData;
import net.connector.iMarketDataEvent;


public class MarketDataListener implements Runnable {
	iMarketDataEvent iMarketDataEvent;	
	FloodControl floodControl;
	int connectionId;
	
	ArrayList<String> tickersURLList = new ArrayList<String>();
	ArrayList<String> tickersList = new ArrayList<String>();
	HashMap<String, MarketData> marketDataHashMap = new HashMap<String, MarketData>();
	
	public MarketDataListener(iMarketDataEvent iMarketDataEvent, FloodControl floodControl, int connectionId){
		this.iMarketDataEvent = iMarketDataEvent;
		this.floodControl = floodControl;
		this.connectionId = connectionId;
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		
		
		tickersURLList.add("https://api.bitflyer.jp/v1/getticker?product_code=BTC_JPY");
		tickersURLList.add("https://api.bitflyer.jp/v1/getticker?product_code=ETH_BTC");
		
		tickersList.add("BTC_JPY");
		tickersList.add("ETH_BTC");
		
		int tickersURLListPosition = 0;
		
		ArrayList<URL> urlList = new ArrayList<URL>();		
		ObjectMapper mapper = new ObjectMapper();
		
		
		try {
			for (int it = 0; it < tickersURLList.size(); it++) {
				urlList.add(new URL(tickersURLList.get(it)));	
				
				String ticker = tickersList.get(it);
				MarketData marketData = new MarketData();
				marketData.setTrdPair(ticker);
				marketDataHashMap.put(ticker, marketData);
			}
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		Random rnd = new Random();
		
		
		while(true){
			
			long mills = Math.abs(rnd.nextLong()) % 100; 
			
			try {
				Thread.sleep(mills);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
			
			synchronized (this.floodControl) {
				int availableRequests = this.floodControl.AvailableRequests();
				
				if(availableRequests > 0){
					String jsonString = null;
					
					
					try {
						URL url = urlList.get(tickersURLListPosition);
						
						HttpsURLConnection httpsURLConnection = (HttpsURLConnection)url.openConnection();
						
						BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpsURLConnection.getInputStream()));
						jsonString = bufferedReader.readLine();
						
						JsonNode jn = mapper.readTree(jsonString);
						BitflyerTicker ticker = mapper.convertValue(jn, BitflyerTicker.class);
							
						
						MarketData marketData = marketDataHashMap.get(tickersList.get(tickersURLListPosition));
						if(marketData.getHighestBid() != ticker.getBest_bid() || marketData.getLowestAsk() != ticker.getBest_ask()) {
							marketData.setLowestAsk(ticker.getBest_ask());
							marketData.setHighestBid(ticker.getBest_bid());

							this.iMarketDataEvent.MarketDataChanged(this.connectionId, marketData);
						}
						
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					tickersURLListPosition = (tickersURLListPosition + 1)% tickersURLList.size();
					this.floodControl.NewRequest();
				}
			}
		}
	}



}

