package net.poloniex;

import java.util.ArrayList;

public class PoloniexAddOrderResult {
	private String orderNumber;
	private ArrayList<PoloniexAddOrderResultingTrade> resultingTrades;
	
	public String getOrderNumber() {
		return orderNumber;
	}
	public void setOrderNumber(String orderNumber) {
		this.orderNumber = orderNumber;
	}
	public ArrayList<PoloniexAddOrderResultingTrade> getResultingTrades() {
		return resultingTrades;
	}
	public void setResultingTrades(ArrayList<PoloniexAddOrderResultingTrade> resultingTrades) {
		this.resultingTrades = resultingTrades;
	}
	
}
