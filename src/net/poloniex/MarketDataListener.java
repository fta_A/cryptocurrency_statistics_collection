package net.poloniex;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Random;

import javax.net.ssl.HttpsURLConnection;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import net.connector.FloodControl;
import net.connector.MarketData;
import net.connector.iMarketDataEvent;


public class MarketDataListener implements Runnable {
	iMarketDataEvent iMarketDataEvent;	
	FloodControl floodControl;
	int connectionId;
	HashMap<String, MarketData> marketDataHashMap = new HashMap<String, MarketData>();
	HashSet<String> subscribedList = new HashSet<String>();

	public MarketDataListener(iMarketDataEvent iMarketDataEvent, FloodControl floodControl, int connectionId){
		this.iMarketDataEvent = iMarketDataEvent;
		this.floodControl = floodControl;
		this.connectionId = connectionId;
	}
	
	public void Subscribe(ArrayList<String> tradingPairsList) {
		synchronized (this.subscribedList) {
			for(String tickerPair : tradingPairsList)
				this.subscribedList.add(tickerPair);
			}
	}
	
	public void Subscribe(String tradingPair) {
		synchronized (this.subscribedList) {			
			this.subscribedList.add(tradingPair);	
		}
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub

		String poloniex_ticker_url =  "https://poloniex.com/public?command=returnTicker";
		URL url = null;
		ObjectMapper mapper = new ObjectMapper();
	
		try {
			url = new URL(poloniex_ticker_url);
		} catch (MalformedURLException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		Random rnd = new Random();
		
		
		while(true){
			
			long mills = Math.abs(rnd.nextLong()) % 100; 
			
			try {
				Thread.sleep(mills);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
			
			synchronized (this.floodControl) {
				int availableRequests = this.floodControl.AvailableRequests();
				
				if(availableRequests > 0){
					String jsonString = null;
					
					
					try {
						HttpsURLConnection httpsURLConnection = (HttpsURLConnection)url.openConnection();						
						BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpsURLConnection.getInputStream()));
						jsonString = bufferedReader.readLine();
		
						TypeReference<Map<String, MarketData>> tr = new TypeReference<Map<String, MarketData>>() { };
						Map<String, MarketData> td = mapper.readValue(jsonString, tr);
						
						for (Map.Entry<String, MarketData> entry : td.entrySet())
						{
							boolean subscribed = false;							
							synchronized (this.subscribedList) {
								subscribed = this.subscribedList.contains(entry.getKey());
							}							
							if(!subscribed) continue;
							
							
							MarketData marketData = marketDataHashMap.get(entry.getKey());
							if(marketData == null) {								
								marketData = entry.getValue();
								marketData.setTrdPair(entry.getKey());
								marketDataHashMap.put(entry.getKey(), marketData);
								
								this.iMarketDataEvent.MarketDataChanged(this.connectionId, entry.getValue());
							}else {
								
								MarketData newMarketData = entry.getValue();
								if(marketData.getHighestBid() != newMarketData.getHighestBid() || marketData.getLowestAsk() != newMarketData.getLowestAsk()) {
									marketData.setLowestAsk(newMarketData.getLowestAsk());
									marketData.setHighestBid(newMarketData.getHighestBid());

									this.iMarketDataEvent.MarketDataChanged(this.connectionId, marketData);									
								}
									
							}
						}

						
						
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					
					this.floodControl.NewRequest();
				}
			}
		}
	}
}
