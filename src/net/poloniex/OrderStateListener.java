package net.poloniex;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.net.ssl.HttpsURLConnection;

import org.apache.commons.codec.binary.Hex;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import net.connector.FloodControl;
import net.connector.Order;
import net.connector.Order.MarketType;
import net.connector.Order.OrderDirection;
import net.connector.Order.OrderState;
import net.connector.iOrderStateEvent;

public class OrderStateListener  implements Runnable {
	iOrderStateEvent iOrderStateEvent;	
	FloodControl floodControl;
	int connectionId;
	
	ObjectMapper mapper = new ObjectMapper();
	String URLString = "https://poloniex.com/tradingApi";
	
	String apiKey;
	String privateKey;

	int requestOrder = 0;
	
	HashMap<Integer, Order> activeOrdersMap;
	HashMap<String, Integer> orderNumToUserrefMap;
	HashMap<String, Order> killedOrdersMap;
	
	ArrayList<Order> updatedOrders;
	ArrayList<Order> errorOrders;
	
	
	ArrayList<Order> ordersToSend;
	ArrayList<Order> newOrders;
	
	
	public OrderStateListener(iOrderStateEvent iOrderStateEvent, FloodControl floodControl, int connectionId){
		this.iOrderStateEvent = iOrderStateEvent;
		this.floodControl = floodControl;
		this.connectionId = connectionId;
		
		this.activeOrdersMap = new HashMap<Integer, Order>();
		this.killedOrdersMap = new HashMap<String, Order>();
		
		this.orderNumToUserrefMap = new HashMap<String, Integer>();
		
		this.updatedOrders = new ArrayList<Order>();
		this.errorOrders = new ArrayList<Order>();
		
		
		this.ordersToSend = new ArrayList<Order>();
		this.newOrders = new ArrayList<Order>();
		
		this.apiKey = "6TPTK5NS-TTTQZMF8-9DTRI5GU-SDORAM96";
		this.privateKey = "b4d962b2e88aba010876bcf2496efed76103f4ec0a351bd729a52b533984a2deb9bb51b7738d5fc57abee9fc9d50b1e426a86424c5a84870829f004084fff78c";

	}
	
	public void NewOrder(Order order) {
		synchronized(this.ordersToSend) {
			this.ordersToSend.add(order);
		}
	}
	
	@Override
	public void run() {
		Random rnd = new Random();
		
		// TODO Auto-generated method stub
		
		while(true){
			
			long mills = Math.abs(rnd.nextLong()) % 100; 
			
			try {
				Thread.sleep(mills);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
			ArrayList<Order> tmpArrayList_Order;
			synchronized(this.ordersToSend) {
				if(this.ordersToSend.size() > 0){
					tmpArrayList_Order = this.ordersToSend;
					this.ordersToSend = this.newOrders;
					this.newOrders = tmpArrayList_Order;
				}
			}
			
			for(Order order: this.newOrders) {
				switch(order.orderType) {
				case NewOrder:
					AddOrder(order);
				break;
				case KillOrder:
					Order activeOrder = this.activeOrdersMap.get(order.userref);
					if(activeOrder != null) KillOrder(activeOrder);
				break;
				default:
					break;
				}
			}			
			this.newOrders.clear();
			
			if(this.activeOrdersMap.size() > 0 || this.killedOrdersMap.size() > 0) {
				synchronized (this.floodControl) {
					int availableRequests = this.floodControl.AvailableRequests();
					
					if(availableRequests > 0){
						if(requestOrder == 0) {
							if(this.activeOrdersMap.size() == 0) requestOrder = (requestOrder + 1) % 2;
						}else if(requestOrder == 1) {
							if(this.killedOrdersMap.size() == 0) requestOrder = (requestOrder + 1) % 2;
						}


						switch(requestOrder) {
						case 0:
							CheckOpenedOrders();
							break;
						case 1:
							CheckClosedOrders();
							break;
						}
						
						
						requestOrder = (requestOrder + 1) % 2;					
						this.floodControl.NewRequest();
					}
				}
			}
			
			for(Order order : this.updatedOrders)
				this.iOrderStateEvent.OrderStateChanged(order);				
			this.updatedOrders.clear();
		}
		
		
	}
	
	
	
	
	private void AddOrder(Order order) {
		String nonce = String.valueOf(System.currentTimeMillis());
		String postData = "nonce=" + nonce;
		if(order.dir == OrderDirection.buy)
			postData += "&command=buy";
		else 
			postData += "&command=sell";

		postData += "&currencyPair=" + order.trdPair;
		postData += "&rate=" + order.price;
		postData += "&amount=" + order.amount;
		
		order.marketType = MarketType.limit;
		
		HttpsURLConnection httpsURLConnection;
		
		try {
			httpsURLConnection = this.preparePostHeader("", nonce, postData);
			OutputStreamWriter writer = new OutputStreamWriter(httpsURLConnection.getOutputStream());
	        writer.write(postData);
	        writer.flush();
			
	        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpsURLConnection.getInputStream()));
	        String jsonString = bufferedReader.readLine();	

	        System.out.println("Poloniex Add Answer: " + jsonString);

	        
	        JsonNode jn = mapper.readTree(jsonString);
	        
	        
	        String error = mapper.convertValue(jn.get("error"), String.class);
	        if(error != null) {
	        	order.orderState = OrderState.Refused;
	        	order.errorState = error;
	        	
	        	this.updatedOrders.add(order);
	        }else {
	        	PoloniexAddOrderResult addOrderResult = mapper.convertValue(jn, PoloniexAddOrderResult.class);
	        	//order.transIdList = addOrderResult.getTxid();
        	
	        	
	        	if(order.marketType == MarketType.market) {
	        		order.orderState = OrderState.Executed;
	        		
		        	for(String txid: order.transIdList) {
		        		this.killedOrdersMap.put(txid, order);
		        	}
	        	}else {
	        		double traded_amt = 0;
	        		for(PoloniexAddOrderResultingTrade trade : addOrderResult.getResultingTrades()) {
	        			traded_amt += trade.getAmount();
	        		}
	        		
	        		order.transIdList.clear();
	        		order.transIdList.add(addOrderResult.getOrderNumber());
	        		
	        		if(order.amount == traded_amt) {
	        			order.amount = 0;
	        			order.orderState = OrderState.Executed;
	        			this.updatedOrders.add(order);
	        		}else {
		        		order.orderState = OrderState.Active;
		        		this.orderNumToUserrefMap.put(addOrderResult.getOrderNumber(), order.userref);		        		
		        		this.activeOrdersMap.put(order.userref, order);
		        		this.updatedOrders.add(order);
	        		}
	        	}

	        }
		
	        
		
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
			this.errorOrders.add(order);
			this.activeOrdersMap.put(order.userref, order);
			//NewOrder(order);
		}
		
		
		
	}
	
	private void KillOrder(Order order) {
		String nonce = String.valueOf(System.currentTimeMillis());
		String postData = "nonce=" + nonce;
		postData += "&command=cancelOrder";
		postData += "&orderNumber=" + order.transIdList.get(0);
		
		
		HttpsURLConnection httpsURLConnection;
		try {
			httpsURLConnection = this.preparePostHeader("", nonce, postData);
			OutputStreamWriter writer = new OutputStreamWriter(httpsURLConnection.getOutputStream());
	        writer.write(postData);
	        writer.flush();
			
	        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpsURLConnection.getInputStream()));
	        String jsonString = bufferedReader.readLine();	
	        
	        System.out.println("Poloniex Kill Answer: " + jsonString);
	        
	        JsonNode jn = mapper.readTree(jsonString);
	        
	        String error = mapper.convertValue(jn.get("error"), String.class);
	        
	        if (error != null) {
	        	if(error.contains("Invalid order number, or you are not the person who placed the order")) {
	        		order.orderState = OrderState.Executed;	   
	        		order.amount = 0;	        		
	        		this.updatedOrders.add(order);
	        		this.activeOrdersMap.remove(order.userref);
	        	}else{
	        		NewOrder(order); 
	        	}	               	
	        }else {
	        	order.orderState = OrderState.Killed;
	        	double amount = mapper.convertValue(jn.get("amount"), double.class);
	        	order.amount = amount;
	        	
	        	
	        	this.activeOrdersMap.remove(order.userref);
	        	this.updatedOrders.add(order);
	        }
	        
	        
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
			NewOrder(order);
		}
	}
	
	private void CheckOpenedOrders() {
		String nonce = String.valueOf(System.currentTimeMillis());
		String postData = "nonce=" + nonce;
		postData += "&command=returnOpenOrders";
		postData += "&currencyPair=all";

		HttpsURLConnection httpsURLConnection;
		try {
			httpsURLConnection = this.preparePostHeader("", nonce, postData);
			OutputStreamWriter writer = new OutputStreamWriter(httpsURLConnection.getOutputStream());
		    writer.write(postData);
		    writer.flush();
			
		    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpsURLConnection.getInputStream()));
		    String jsonString = bufferedReader.readLine();	
		    JsonNode jn = mapper.readTree(jsonString);
	       
		    String error = mapper.convertValue(jn.get("error"), String.class);
	        if(error != null) {
	        	
	        }else {
	        	
	    		TypeReference<Map<String, ArrayList<PoloniexOrderStateResult>>> tr = new TypeReference<Map<String, ArrayList<PoloniexOrderStateResult>>>() { };
	    		Map<String, ArrayList<PoloniexOrderStateResult>> td = mapper.convertValue(jn, tr);	    		
	    		for (Map.Entry<String, ArrayList<PoloniexOrderStateResult>> entry : td.entrySet())
	    		{
	    			for(PoloniexOrderStateResult ticker : entry.getValue()) {
	    				if(this.orderNumToUserrefMap.containsKey(ticker.getOrderNumber())) {
	    				
			    			int userref = this.orderNumToUserrefMap.get(ticker.getOrderNumber());
			    			Order order = this.activeOrdersMap.get(userref);
			    			
			    			if(order != null) {
			    				if(order.orderState == OrderState.Adding) {
			    					order.orderState = OrderState.Active;
			    					
			    					if(order.transIdList == null) order.transIdList = new ArrayList<String>();
			    					order.transIdList.add(ticker.getOrderNumber());
			    					
			    					order.amount = 0; 
								}
			    				
			    				double residual = ticker.getAmount();		    			
				    			if(order.amount != residual) {
				    				order.amount = residual;
				    				this.iOrderStateEvent.OrderStateChanged(order);	
				    			}
								
				    			
				    			
			    			}
	    				}
	    			}
	    			
	    		}
	        }
	    
	        for(Order order : this.errorOrders) {
	        	if(order.orderState == OrderState.Adding)
	        		this.NewOrder(order);
			}
	        
	        this.errorOrders.clear();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
	}
	
	private void CheckClosedOrders() {
		
	}
	
	private HttpsURLConnection preparePostHeader (String pathURLString, String nonce, String postData) throws IOException {
		String fullURLString = this.URLString + pathURLString;
		URL postURL = new URL(fullURLString);
		HttpsURLConnection httpsURLConnection = (HttpsURLConnection)postURL.openConnection();
		httpsURLConnection.setRequestMethod("POST");
		httpsURLConnection.setRequestProperty("Key", this.apiKey);
		
		String calculatedSignature = this.calculateSignature(pathURLString, nonce, postData);
		httpsURLConnection.setRequestProperty("Sign", calculatedSignature);
		httpsURLConnection.setDoOutput(true);
		return httpsURLConnection;
	}
	
	private String calculateSignature(String uriPath, String nonce, String postData) {
	    String signature = "";
	    try {
			Mac mac = Mac.getInstance("HmacSHA512");
			mac.init(new SecretKeySpec(this.privateKey.getBytes(), "HmacSHA512"));
			signature = new String(Hex.encodeHex((mac.doFinal(postData.getBytes()))));
	    	
	    	
	    	
	    } catch(Exception e) {}
	    return signature;		
	}

}
