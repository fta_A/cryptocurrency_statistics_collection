package net.poloniex;

public class PoloniexAddOrderResultingTrade {
	private double amount;
	private String date;
	private double rate;
	private double total;
	private Long tradeID;
	private String type;
	
	
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public String getDate() {
		return date;
	}
	public void setDate(String date) {
		this.date = date;
	}
	public double getRate() {
		return rate;
	}
	public void setRate(double rate) {
		this.rate = rate;
	}
	public double getTotal() {
		return total;
	}
	public void setTotal(double total) {
		this.total = total;
	}
	public Long getTradeID() {
		return tradeID;
	}
	public void setTradeID(Long tradeID) {
		this.tradeID = tradeID;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	
}
