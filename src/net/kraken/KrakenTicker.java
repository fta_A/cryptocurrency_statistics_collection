package net.kraken;

import java.util.ArrayList;

public class KrakenTicker {
	private ArrayList<Double> a;
	private ArrayList<Double> b;
	private ArrayList<Double> c;
	private ArrayList<Double> v;
	private ArrayList<Double> p;
	private ArrayList<Double> t;
	private ArrayList<Double> l;
	private ArrayList<Double> h;
	private Double o;
	
	public ArrayList<Double> getA() {
		return a;
	}
	public void setA(ArrayList<Double> a) {
		this.a = a;
	}
	public ArrayList<Double> getB() {
		return b;
	}
	public void setB(ArrayList<Double> b) {
		this.b = b;
	}
	public ArrayList<Double> getC() {
		return c;
	}
	public void setC(ArrayList<Double> c) {
		this.c = c;
	}
	public ArrayList<Double> getV() {
		return v;
	}
	public void setV(ArrayList<Double> v) {
		this.v = v;
	}
	public ArrayList<Double> getP() {
		return p;
	}
	public void setP(ArrayList<Double> p) {
		this.p = p;
	}
	public ArrayList<Double> getT() {
		return t;
	}
	public void setT(ArrayList<Double> t) {
		this.t = t;
	}
	public ArrayList<Double> getL() {
		return l;
	}
	public void setL(ArrayList<Double> l) {
		this.l = l;
	}
	public ArrayList<Double> getH() {
		return h;
	}
	public void setH(ArrayList<Double> h) {
		this.h = h;
	}
	public Double getO() {
		return o;
	}
	public void setO(Double o) {
		this.o = o;
	}
	
	
}
