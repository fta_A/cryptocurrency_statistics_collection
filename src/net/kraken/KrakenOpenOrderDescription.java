package net.kraken;

public class KrakenOpenOrderDescription {
	private String pair;
	private String type;
	private String ordertype;
	private double price;
	private double price2;
	private String leverage;
	private String order;
	
	
	public String getPair() {
		return pair;
	}
	public void setPair(String pair) {
		this.pair = pair;
	}
	public String getType() {
		return type;
	}
	public void setType(String type) {
		this.type = type;
	}
	public String getOrdertype() {
		return ordertype;
	}
	public void setOrdertype(String ordertype) {
		this.ordertype = ordertype;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public double getPrice2() {
		return price2;
	}
	public void setPrice2(double price2) {
		this.price2 = price2;
	}
	public String getLeverage() {
		return leverage;
	}
	public void setLeverage(String leverage) {
		this.leverage = leverage;
	}
	public String getOrder() {
		return order;
	}
	public void setOrder(String order) {
		this.order = order;
	}
	
	

}
