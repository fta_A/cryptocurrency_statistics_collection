package net.kraken;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Random;

import javax.net.ssl.HttpsURLConnection;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import net.connector.FloodControl;
import net.connector.MarketData;
import net.connector.iMarketDataEvent;


public class MarketDataListener implements Runnable {
	iMarketDataEvent iMarketDataEvent;	
	FloodControl floodControl;
	int connectionId;
	HashMap<String, MarketData> marketDataHashMap = new HashMap<String, MarketData>();
	HashSet<String> subscribedList = new HashSet<String>();

	public MarketDataListener(iMarketDataEvent iMarketDataEvent, FloodControl floodControl, int connectionId){
		this.iMarketDataEvent = iMarketDataEvent;
		this.floodControl = floodControl;
		this.connectionId = connectionId;
	}
	
	public void Subscribe(ArrayList<String> tradingPairsList) {
		synchronized (this.subscribedList) {
			for(String tickerPair : tradingPairsList)
				this.subscribedList.add(tickerPair);
			}
	}
	
	public void Subscribe(String tradingPair) {
		synchronized (this.subscribedList) {			
			this.subscribedList.add(tradingPair);	
		}
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub

		String kraken_ticker_url =  "https://api.kraken.com/0/public/Ticker";
		URL url = null;
		ObjectMapper mapper = new ObjectMapper();
		
		try {
			kraken_ticker_url += GetTradingPairsList();			
			url = new URL(kraken_ticker_url);
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		Random rnd = new Random();
		
		
		while(true){
			
			long mills = Math.abs(rnd.nextLong()) % 100; 
			
			try {
				Thread.sleep(mills);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
			
			synchronized (this.floodControl) {
				int availableRequests = this.floodControl.AvailableRequests();
				
				if(availableRequests > 0){
					String jsonString = null;
					
					
					try {
						HttpsURLConnection httpsURLConnection = (HttpsURLConnection)url.openConnection();						
						
						BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpsURLConnection.getInputStream()));
						jsonString = bufferedReader.readLine();
						
						JsonNode jn = mapper.readTree(jsonString);
		
						TypeReference<Map<String, KrakenTicker>> tr = new TypeReference<Map<String, KrakenTicker>>() { };
						Map<String, KrakenTicker> td = mapper.convertValue(jn.get("result"), tr);
						
						for (Map.Entry<String, KrakenTicker> entry : td.entrySet())
						{
							
							
							boolean subscribed = false;							
							synchronized (this.subscribedList) {
								subscribed = this.subscribedList.contains(entry.getKey());
							}							
							if(!subscribed) continue;

							KrakenTicker ticker = entry.getValue();	
							
							double ask = ticker.getA().size() == 0 ? 0 : ticker.getA().get(0);
							double bid = ticker.getB().size() == 0 ? 0 : ticker.getB().get(0);

							
							MarketData marketData = marketDataHashMap.get(entry.getKey());
							if(marketData.getHighestBid() != bid || marketData.getLowestAsk() != ask) {
								marketData.setLowestAsk(ask);
								marketData.setHighestBid(bid);

								this.iMarketDataEvent.MarketDataChanged(this.connectionId, marketData);
							}
						}

						
						
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					
					this.floodControl.NewRequest();
				}
			}
		}
	}

	private String GetTradingPairsList() throws IOException {
		String kraken_ticker_url =  "https://api.kraken.com/0/public/AssetPairs";
		URL url = new URL(kraken_ticker_url);
		ObjectMapper mapper = new ObjectMapper();
		String tradingPairs = "";

		synchronized (this.floodControl) {
			int availableRequests = this.floodControl.AvailableRequests();
			
			if(availableRequests > 0){
				String jsonString = null;
				
				
				HttpsURLConnection httpsURLConnection = (HttpsURLConnection)url.openConnection();						
				
				BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpsURLConnection.getInputStream()));
				jsonString = bufferedReader.readLine();
				
				JsonNode jn = mapper.readTree(jsonString);
				
				TypeReference<Map<String, KrakenAssetPair>> tr = new TypeReference<Map<String, KrakenAssetPair>>() { };
				Map<String, KrakenAssetPair> td = mapper.convertValue(jn.get("result"), tr);
				
				for (Map.Entry<String, KrakenAssetPair> entry : td.entrySet())
				{
				    //System.out.println(entry.getKey() + ". ID = " + entry.getValue().getAltname());
					//KrakenAssetPair kap = entry.getValue();
					//System.out.println(kap.getAltname() + ";" + kap.getBase() + ";" + kap.getQuote());
					
					tradingPairs += "," + entry.getKey();
					
					String ticker = entry.getKey();					
					MarketData marketData = new MarketData();
					marketData.setTrdPair(ticker);
					marketDataHashMap.put(ticker, marketData);

				}				
				
				this.floodControl.NewRequest();

			}
		}
		
		if(tradingPairs.length() > 0) {
			tradingPairs = "?pair=" + tradingPairs.substring(1);
		}
		
		return tradingPairs;
	}
}

