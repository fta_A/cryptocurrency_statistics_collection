package net.kraken;

public class KrakenAssetPair {
	private String altname;
	private String aclass_base;
	private String base;
	private String aclass_quote;
	private String quote;
	private String lot;
	private int pair_decimals;
	private int lot_decimals;
	private int lot_multiplier;
	
	private Object leverage_buy;
	private Object leverage_sell;
	
	private Object fees;
	private Object fees_maker;
	
	private String fee_volume_currency;
	private int margin_call;
	private int margin_stop;
	
	public String getAltname() {
		return altname;
	}
	public void setAltname(String altname) {
		this.altname = altname;
	}
	public String getAclass_base() {
		return aclass_base;
	}
	public void setAclass_base(String aclass_base) {
		this.aclass_base = aclass_base;
	}
	public String getBase() {
		return base;
	}
	public void setBase(String base) {
		this.base = base;
	}
	public String getAclass_quote() {
		return aclass_quote;
	}
	public void setAclass_quote(String aclass_quote) {
		this.aclass_quote = aclass_quote;
	}
	public String getQuote() {
		return quote;
	}
	public void setQuote(String quote) {
		this.quote = quote;
	}
	public String getLot() {
		return lot;
	}
	public void setLot(String lot) {
		this.lot = lot;
	}
	public int getPair_decimals() {
		return pair_decimals;
	}
	public void setPair_decimals(int pair_decimals) {
		this.pair_decimals = pair_decimals;
	}
	public int getLot_decimals() {
		return lot_decimals;
	}
	public void setLot_decimals(int lot_decimals) {
		this.lot_decimals = lot_decimals;
	}
	public int getLot_multiplier() {
		return lot_multiplier;
	}
	public void setLot_multiplier(int lot_multiplier) {
		this.lot_multiplier = lot_multiplier;
	}
	public Object getLeverage_buy() {
		return leverage_buy;
	}
	public void setLeverage_buy(Object leverage_buy) {
		this.leverage_buy = leverage_buy;
	}
	public Object getLeverage_sell() {
		return leverage_sell;
	}
	public void setLeverage_sell(Object leverage_sell) {
		this.leverage_sell = leverage_sell;
	}
	public Object getFees() {
		return fees;
	}
	public void setFees(Object fees) {
		this.fees = fees;
	}
	public Object getFees_maker() {
		return fees_maker;
	}
	public void setFees_maker(Object fees_maker) {
		this.fees_maker = fees_maker;
	}
	public String getFee_volume_currency() {
		return fee_volume_currency;
	}
	public void setFee_volume_currency(String fee_volume_currency) {
		this.fee_volume_currency = fee_volume_currency;
	}
	public int getMargin_call() {
		return margin_call;
	}
	public void setMargin_call(int margin_call) {
		this.margin_call = margin_call;
	}
	public int getMargin_stop() {
		return margin_stop;
	}
	public void setMargin_stop(int margin_stop) {
		this.margin_stop = margin_stop;
	}
	
}
