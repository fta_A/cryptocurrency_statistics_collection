package net.kraken;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.security.MessageDigest;
import java.util.ArrayList;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;
import java.util.Random;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.net.ssl.HttpsURLConnection;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import net.connector.FloodControl;
import net.connector.Order;
import net.connector.Order.MarketType;
import net.connector.Order.OrderState;
import net.connector.iOrderStateEvent;

public class OrderStateListener  implements Runnable {
	
	iOrderStateEvent iOrderStateEvent;	
	FloodControl floodControl;
	int connectionId;
	
	ObjectMapper mapper = new ObjectMapper();
	
	String URLString = "https://api.kraken.com";
	String addOrderPathString = "/0/private/AddOrder";
	String cancelOrderPathString = "/0/private/CancelOrder";
	String openedOrdersPathString = "/0/private/OpenOrders";
	String closedOrdersPathString = "/0/private/ClosedOrders";
	
	String apiKey;
	String privateKey;

	int requestOrder = 0;
	
	HashMap<Integer, Order> activeOrdersMap;
	HashMap<String, Order> killedOrdersMap;
	
	ArrayList<Order> updatedOrders;
	ArrayList<Order> errorOrders;
	HashMap<Integer, Order> lostOrders;
	
	ArrayList<Order> ordersToSend;
	ArrayList<Order> newOrders;
	
	public OrderStateListener(iOrderStateEvent iOrderStateEvent, FloodControl floodControl, int connectionId){
		this.iOrderStateEvent = iOrderStateEvent;
		this.floodControl = floodControl;
		this.connectionId = connectionId;
		
		this.activeOrdersMap = new HashMap<Integer, Order>();
		this.killedOrdersMap = new HashMap<String, Order>();
		
		this.updatedOrders = new ArrayList<Order>();
		this.errorOrders = new ArrayList<Order>();
		
		
		this.ordersToSend = new ArrayList<Order>();
		this.newOrders = new ArrayList<Order>();
		
		this.lostOrders = new HashMap<Integer, Order>();
		
		this.apiKey = "sX4fQcjBimSnG1hcTDTQIii8nqE1aufrF4/uXiLtS2VKNAjqNkrcplo3";
		this.privateKey = "8vri6kR878738scIPa8IqfBpbri7U4aMhFO7/xE2wtcQ5k9iQFV4eb6V8Tyfz4d4Ju7mCk1XBRvTinz827zoBg==";

	}
	
	
	public void NewOrder(Order order) {
		synchronized(this.ordersToSend) {
			this.ordersToSend.add(order);
		}
	}
	
	
	
	
	@Override
	public void run() {
		Random rnd = new Random();
		
		// TODO Auto-generated method stub
		
		while(true){
			long mills = Math.abs(rnd.nextLong()) % 100; 
			
			try {
				Thread.sleep(mills);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
			ArrayList<Order> tmpArrayList_Order;
			synchronized(this.ordersToSend) {
				if(this.ordersToSend.size() > 0){
					tmpArrayList_Order = this.ordersToSend;
					this.ordersToSend = this.newOrders;
					this.newOrders = tmpArrayList_Order;
				}
			}
			
			for(Order order: this.newOrders) {
				switch(order.orderType) {
				case NewOrder:
					AddOrder(order);
				break;
				case KillOrder:
					Order activeOrder = this.activeOrdersMap.get(order.userref);
					if(activeOrder != null) KillOrder(activeOrder);
				break;
				default:
					break;
				}
			}			
			this.newOrders.clear();
			
			if(this.activeOrdersMap.size() > 0 || this.killedOrdersMap.size() > 0) {
				synchronized (this.floodControl) {
					int availableRequests = this.floodControl.AvailableRequests();
					
					if(availableRequests > 0){
						if(requestOrder == 0) {
							if(this.activeOrdersMap.size() == 0) requestOrder = (requestOrder + 1) % 2;
						}else if(requestOrder == 1) {
							if(this.killedOrdersMap.size() == 0) requestOrder = (requestOrder + 1) % 2;
						}


						switch(requestOrder) {
						case 0:
							CheckOpenedOrders();
							break;
						case 1:
							CheckClosedOrders();
							break;
						}
						
						
						requestOrder = (requestOrder + 1) % 2;					
						this.floodControl.NewRequest();
					}
				}
			}
			
			for(Order order : this.updatedOrders)
				this.iOrderStateEvent.OrderStateChanged(order);				
			this.updatedOrders.clear();
			
			/*
			for(Order order : this.refusedOrders)
				this.iOrderStateEvent.OrderStateChanged(order);				
			this.refusedOrders.clear();
			
			for(Order order : this.executedOrders)
				this.iOrderStateEvent.OrderStateChanged(order);	
			this.executedOrders.clear();
				*/
			

		}
	}
	
	private void CheckOpenedOrders() {
		String nonce = String.valueOf(System.currentTimeMillis());
		String postData = "nonce=" + nonce;

		HttpsURLConnection httpsURLConnection;
		try {
			httpsURLConnection = this.preparePostHeader(this.openedOrdersPathString, nonce, postData);
			OutputStreamWriter writer = new OutputStreamWriter(httpsURLConnection.getOutputStream());
		    writer.write(postData);
		    writer.flush();
			
		    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpsURLConnection.getInputStream()));
		    String jsonString = bufferedReader.readLine();	
		    JsonNode jn = mapper.readTree(jsonString);
	       
		    TypeReference<ArrayList<String>> listTypeReference = new TypeReference<ArrayList<String>> () {};
	        ArrayList<String> errorList = mapper.convertValue(jn.get("error"), listTypeReference);
	        if(errorList.size() > 0) {
	        	
	        }else {
	        	
	    		TypeReference<Map<String, KrakenOpenOrderResult>> tr = new TypeReference<Map<String, KrakenOpenOrderResult>>() { };
	    		Map<String, KrakenOpenOrderResult> td = mapper.convertValue(jn.get("result").get("open"), tr);	    		
	    		for (Map.Entry<String, KrakenOpenOrderResult> entry : td.entrySet())
	    		{
	    			KrakenOpenOrderResult ticker = entry.getValue();
	    			
	    			Order order = this.activeOrdersMap.get(ticker.getUserref());
	    			
	    			if(order != null) {
	    				if(order.orderState == OrderState.Adding) {
	    					order.orderState = OrderState.Active;
	    					
	    					if(order.transIdList == null) order.transIdList = new ArrayList<String>();
	    					order.transIdList.add(entry.getKey());
	    					
	    					order.amount = 0; 
						}
	    				
	    				double residual = ticker.getVol() - ticker.getVol_exec();		    			
		    			if(order.amount != residual) {
		    				order.amount = residual;
		    				this.iOrderStateEvent.OrderStateChanged(order);	
		    			}
						
		    			
		    			
	    			}
	    			
	    		}
	        }
	    
	        for(Order order : this.errorOrders) {
	        	if(order.orderState == OrderState.Adding) {
	        		System.out.println("Order " + order.userref + " not found. Trying one more time...");
	        				
	        	
	        		this.lostOrders.put(order.userref, order);
	        		//this.NewOrder(order);
	        	}
			}
	        
	        this.errorOrders.clear();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		
		
		
	}
	
	private void CheckClosedOrders() {
		String nonce = String.valueOf(System.currentTimeMillis());
		String postData = "nonce=" + nonce;
		
		for (String txid : this.killedOrdersMap.keySet()) {
			postData += "&start=" + txid;
		}
		
		for(Order order : this.lostOrders.values()) {
			postData += "&start=" + order.sentUnixTime;
		}

		HttpsURLConnection httpsURLConnection;
		try {
			httpsURLConnection = this.preparePostHeader(this.closedOrdersPathString, nonce, postData);
			OutputStreamWriter writer = new OutputStreamWriter(httpsURLConnection.getOutputStream());
		    writer.write(postData);
		    writer.flush();
			
		    BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpsURLConnection.getInputStream()));
		    String jsonString = bufferedReader.readLine();	
		    JsonNode jn = mapper.readTree(jsonString);
	       
		    TypeReference<ArrayList<String>> listTypeReference = new TypeReference<ArrayList<String>> () {};
	        ArrayList<String> errorList = mapper.convertValue(jn.get("error"), listTypeReference);
	        if(errorList.size() > 0) {
	        	
	        }else {
	        	
	    		TypeReference<Map<String, KrakenOpenOrderResult>> tr = new TypeReference<Map<String, KrakenOpenOrderResult>>() { };
	    		Map<String, KrakenOpenOrderResult> td = mapper.convertValue(jn.get("result").get("closed"), tr);	    		
	    		for (Map.Entry<String, KrakenOpenOrderResult> entry : td.entrySet())
	    		{
	    			KrakenOpenOrderResult ticker = entry.getValue();
	    			
	    			Order order = this.killedOrdersMap.get(entry.getKey());
	    			
	    			if(order != null) {
	    				
		    			double residual = ticker.getVol() - ticker.getVol_exec();
	    				order.amount = residual;
	    				this.iOrderStateEvent.OrderStateChanged(order);	
	    			}
	    			
	    			order = this.lostOrders.get(ticker.getUserref());
	    			if(order != null) {
	    				double residual = ticker.getVol() - ticker.getVol_exec();
	    				order.amount = residual;
	    				this.iOrderStateEvent.OrderStateChanged(order);	
	    				
	    				this.lostOrders.remove(order.userref);
	    			}
	    		}
	    		
	    		this.killedOrdersMap.clear();
	        }
	    
		    
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
		if(this.lostOrders.size() > 0) {
			System.out.println("!!! There are lost orders: " + this.lostOrders.size());
		}

	}
	
	
	private void KillOrder(Order order) {
		String nonce = String.valueOf(System.currentTimeMillis());
		String postData = "nonce=" + nonce;
		postData += "&txid=" + order.userref;
		
		
		HttpsURLConnection httpsURLConnection;
		try {
			httpsURLConnection = this.preparePostHeader(this.cancelOrderPathString, nonce, postData);
			OutputStreamWriter writer = new OutputStreamWriter(httpsURLConnection.getOutputStream());
	        writer.write(postData);
	        writer.flush();
			
	        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpsURLConnection.getInputStream()));
	        String jsonString = bufferedReader.readLine();	
	        JsonNode jn = mapper.readTree(jsonString);
	        
	        TypeReference<ArrayList<String>> listTypeReference = new TypeReference<ArrayList<String>> () {};
	        ArrayList<String> errorList = mapper.convertValue(jn.get("error"), listTypeReference);
	        if(errorList.size() > 0) {
	        	System.out.println("Kraken: " + errorList.get(0));
	        	
	        	if(errorList.get(0) == "EService:Unavailable") {
	        		NewOrder(order);
	        	}else if(errorList.get(0).contains("EOrder:Unknown order")) {
	        		order.orderState = OrderState.Executed;	   
	        		order.amount = 0;	        		
	        		this.updatedOrders.add(order);	  
	        		
	        		this.activeOrdersMap.remove(order.userref);
	        	}else {
	        		NewOrder(order);
	        	}
	        }else {
	        	this.activeOrdersMap.remove(order.userref);
	        	
	        	order.orderState = OrderState.Killed;
	        	
	        	for(String txid: order.transIdList) {
	        		this.killedOrdersMap.put(txid, order);
	        	}
	        }
	        
	        
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
			NewOrder(order);
		}
	}
	
	
	private void AddOrder(Order order) {
		String nonce = String.valueOf(System.currentTimeMillis());
		String postData = "nonce=" + nonce;
		postData += "&pair=" + order.trdPair;
		postData += "&type=" + order.dir;
		postData += "&ordertype=" + order.marketType;
		//postData += "&ordertype=limit";
		postData += "&price=" + order.price;
		postData += "&volume=" + order.amount;
		postData += "&userref=" + order.userref;
		//postData += "&trading_agreement = agree";
		
		order.sentUnixTime = System.currentTimeMillis() / 1000L;
		
		HttpsURLConnection httpsURLConnection;
		
		try {
			httpsURLConnection = this.preparePostHeader(this.addOrderPathString, nonce, postData);
			OutputStreamWriter writer = new OutputStreamWriter(httpsURLConnection.getOutputStream());
	        writer.write(postData);
	        writer.flush();
			
	        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpsURLConnection.getInputStream()));
	        String jsonString = bufferedReader.readLine();
	        
	        System.out.println("Kraken Add Order Res: " + jsonString);

	        
	        JsonNode jn = mapper.readTree(jsonString);
	        
	        TypeReference<ArrayList<String>> listTypeReference = new TypeReference<ArrayList<String>> () {};
	        ArrayList<String> errorList = mapper.convertValue(jn.get("error"), listTypeReference);
	        if(errorList.size() > 0) {
	        	if(errorList.get(0).contains("EService:Unavailable")) {
	        		NewOrder(order);
	        	}else {	        	
		        	order.orderState = OrderState.Refused;
		        	order.errorState = errorList.get(0);		        	
		        	this.updatedOrders.add(order);
	        	}
	        }else {
	        	KrakenAddOrderResult addOrderResult = mapper.convertValue(jn.get("result"), KrakenAddOrderResult.class);
	        	order.transIdList.clear();
	        	order.transIdList = addOrderResult.getTxid();
        	
	        	
	        	if(order.marketType == MarketType.market) {
	        		order.orderState = OrderState.Executed;
	        		order.amount = 0;
	        		
		        	for(String txid: order.transIdList) {
		        		this.killedOrdersMap.put(txid, order);
		        	}
	        	}else {
	        		order.orderState = OrderState.Active;
	        		this.activeOrdersMap.put(order.userref, order);
	        		this.updatedOrders.add(order);
	        	}

	        }
		
	        
		
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			
			this.errorOrders.add(order);
			this.activeOrdersMap.put(order.userref, order);
			//NewOrder(order);
		}
		
		
		
	}
	
	private HttpsURLConnection preparePostHeader (String pathURLString, String nonce, String postData) throws IOException {
		String fullURLString = this.URLString + pathURLString;
		URL postURL = new URL(fullURLString);
		HttpsURLConnection httpsURLConnection = (HttpsURLConnection)postURL.openConnection();
		httpsURLConnection.setRequestMethod("POST");
		httpsURLConnection.setRequestProperty("API-Key", this.apiKey);
		
		String calculatedSignature = this.calculateSignature(pathURLString, nonce, postData);
		httpsURLConnection.setRequestProperty("API-Sign", calculatedSignature);
		httpsURLConnection.setDoOutput(true);
		return httpsURLConnection;
	}
	
	private String calculateSignature(String uriPath, String nonce, String postData) {
	    String signature = "";
	    try {
	        MessageDigest md = MessageDigest.getInstance("SHA-256");
	        md.update((nonce + postData).getBytes());
	        Mac mac = Mac.getInstance("HmacSHA512");
	        mac.init(new SecretKeySpec(Base64.getDecoder().decode(this.privateKey.getBytes()), "HmacSHA512"));
	        mac.update(uriPath.getBytes());
	        signature = new String(Base64.getEncoder().encode(mac.doFinal(md.digest())));
	    } catch(Exception e) {}
	    return signature;		
	}
}
