package net.kraken;

public class KrakenOpenOrderResult {
	private String refid;
	private int userref;
	private String status;
	private double opentm;
	private double starttm;
	private double expiretm;
	private KrakenOpenOrderDescription descr;
	private double vol;
	private double vol_exec;
	private double cost;
	private double fee;
	private double price;
	private String misc;
	private String oflags;
	private double closetm;
	private String reason;
	
	
	public String getRefid() {
		return refid;
	}
	public void setRefid(String refid) {
		this.refid = refid;
	}
	public int getUserref() {
		return userref;
	}
	public void setUserref(int userref) {
		this.userref = userref;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public double getOpentm() {
		return opentm;
	}
	public void setOpentm(double opentm) {
		this.opentm = opentm;
	}
	public double getStarttm() {
		return starttm;
	}
	public void setStarttm(double starttm) {
		this.starttm = starttm;
	}
	public double getExpiretm() {
		return expiretm;
	}
	public void setExpiretm(double expiretm) {
		this.expiretm = expiretm;
	}
	public KrakenOpenOrderDescription getDescr() {
		return descr;
	}
	public void setDescr(KrakenOpenOrderDescription descr) {
		this.descr = descr;
	}
	public double getVol() {
		return vol;
	}
	public void setVol(double vol) {
		this.vol = vol;
	}
	public double getVol_exec() {
		return vol_exec;
	}
	public void setVol_exec(double vol_exec) {
		this.vol_exec = vol_exec;
	}
	public double getCost() {
		return cost;
	}
	public void setCost(double cost) {
		this.cost = cost;
	}
	public double getFee() {
		return fee;
	}
	public void setFee(double fee) {
		this.fee = fee;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public String getMisc() {
		return misc;
	}
	public void setMisc(String misc) {
		this.misc = misc;
	}
	public String getOflags() {
		return oflags;
	}
	public void setOflags(String oflags) {
		this.oflags = oflags;
	}
	public double getClosetm() {
		return closetm;
	}
	public void setClosetm(double closetm) {
		this.closetm = closetm;
	}
	public String getReason() {
		return reason;
	}
	public void setReason(String reason) {
		this.reason = reason;
	}
	
	
	
}
