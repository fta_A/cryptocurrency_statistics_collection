package net.kraken;

import java.util.ArrayList;

public class KrakenAddOrderResult {
	private KrakenAddOrderDescription descr;
	private ArrayList<String> txid;
	
	public KrakenAddOrderDescription getDescr() {
		return descr;
	}
	public void setDescr(KrakenAddOrderDescription descr) {
		this.descr = descr;
	}
	public ArrayList<String> getTxid() {
		return txid;
	}
	public void setTxid(ArrayList<String> txid) {
		this.txid = txid;
	}
	
	
}
