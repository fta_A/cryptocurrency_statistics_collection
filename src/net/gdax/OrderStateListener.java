package net.gdax;

import java.util.Random;

import net.connector.FloodControl;
import net.connector.Order;
import net.connector.iOrderStateEvent;

public class OrderStateListener  implements Runnable {
	iOrderStateEvent iOrderStateEvent;	
	FloodControl floodControl;
	int connectionId;
	
	
	public OrderStateListener(iOrderStateEvent iOrderStateEvent, FloodControl floodControl, int connectionId){
		this.iOrderStateEvent = iOrderStateEvent;
		this.floodControl = floodControl;
		this.connectionId = connectionId;
	}
	
	@Override
	public void run() {
		Random rnd = new Random();
		
		// TODO Auto-generated method stub
		Order orderState = new Order();
		
		while(true){
			
			long mills = Math.abs(rnd.nextLong()) % 100; 
			
			try {
				Thread.sleep(mills);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
			synchronized (this.floodControl) {
				int availableRequests = this.floodControl.AvailableRequests();
				
				if(availableRequests > 0){				
					
					
					
					
	
					this.iOrderStateEvent.OrderStateChanged(orderState);					
					this.floodControl.NewRequest();
				}
				
				
			}
			
			
		}
		
		
	}

}
