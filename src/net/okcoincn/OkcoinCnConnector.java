package net.okcoincn;

import java.util.Enumeration;
import java.util.Vector;

import net.connector.ConnectionState;
import net.connector.FloodControl;
import net.connector.MarketData;
import net.connector.Order;
import net.connector.iConnectionEvent;
import net.connector.iExchangeConnector;
import net.connector.iMarketDataEvent;
import net.connector.iOrderStateEvent;

public class OkcoinCnConnector implements iExchangeConnector, iConnectionEvent, iMarketDataEvent, iOrderStateEvent {
	protected Vector<iConnectionEvent> _connectionStateListeners;
	protected Vector<iMarketDataEvent> _marketDataListeners;
	protected Vector<iOrderStateEvent> _orderStateListeners;
	
	String connectionString;
	String exchangeCode;

	ConnectionStateListener connectionStateListener;
	MarketDataListener marketDataListener;
	OrderStateListener orderStateListener;
	
	Thread connectionStateListenerThread;
	Thread marketDataListenerThread;
	Thread orderStateListenerThread;

	FloodControl floodControl;
	int connectionId;
	
	
	public OkcoinCnConnector(int connectionId){
		this.connectionId = connectionId;
		
		this.floodControl = new FloodControl();
		
		this.connectionStateListener = new ConnectionStateListener(this, this.floodControl, this.connectionId);		
		this.connectionStateListenerThread = new Thread(this.connectionStateListener);
		
		this.marketDataListener = new MarketDataListener(this, this.floodControl, this.connectionId);
		this.marketDataListenerThread = new Thread(this.marketDataListener);
		
		this.orderStateListener = new OrderStateListener(this, this.floodControl, this.connectionId);
		this.orderStateListenerThread = new Thread(this.orderStateListener);
		
	}
	
	@Override
	public void Connect() {
		// TODO Auto-generated method stub		
		
		String[] tokens = connectionString.split(";");
		for (String t : tokens) {
			String[] param = t.split("=");
			switch(param[0]) {
			case "FLOOD_PERIOD":
				this.floodControl.floodPeriodSec = Integer.parseInt(param[1]);
				break;
			case "FLOOD_MSG":
				this.floodControl.maxFloodRequests = Integer.parseInt(param[1]);
				break;
			case "EXC_CODE":
				this.exchangeCode = param[1];
				break;
			}
		}
		
		//this.connectionStateListenerThread.start();
		this.marketDataListenerThread.start();
		//this.orderStateListenerThread.start();
	}

	@Override
	public void Disconnect() {
		// TODO Auto-generated method stub

	}

	@Override
	public void AddConnectionEventListener(iConnectionEvent listener) {
		// TODO Auto-generated method stub		
		if (_connectionStateListeners == null)
			_connectionStateListeners = new Vector<iConnectionEvent>();             
		_connectionStateListeners.addElement(listener);		
	}
	
	@Override
	public void AddMarketDataListener(iMarketDataEvent listener) {
		if (_marketDataListeners == null)
			_marketDataListeners = new Vector<iMarketDataEvent>();             
		_marketDataListeners.addElement(listener);		
		
	}
	
	@Override
	public void AddOrderStateListener(iOrderStateEvent listener) {
		// TODO Auto-generated method stub
		
		if (_orderStateListeners == null)
			_orderStateListeners = new Vector<iOrderStateEvent>();             
		_orderStateListeners.addElement(listener);		
	}



	@Override
	public void ConnectionStateChanged(ConnectionState connectionState) {
		// TODO Auto-generated method stub
		if (_connectionStateListeners == null || _connectionStateListeners.isEmpty()) return;
		
        
        Enumeration<iConnectionEvent> e = _connectionStateListeners.elements();
        while (e.hasMoreElements())
        {
        	iConnectionEvent iConnectionEvent = (iConnectionEvent)e.nextElement();
        	iConnectionEvent.ConnectionStateChanged(connectionState);
        }
	}

	@Override
	public void MarketDataChanged(int connectionId, MarketData marketData) {
		// TODO Auto-generated method stub
		
		if (_marketDataListeners == null || _marketDataListeners.isEmpty()) return;
		
        
        Enumeration<iMarketDataEvent> e = _marketDataListeners.elements();
        while (e.hasMoreElements())
        {
        	iMarketDataEvent iMarketDataEvent = (iMarketDataEvent)e.nextElement();
        	iMarketDataEvent.MarketDataChanged(connectionId, marketData);
        }
	}

	@Override
	public void OrderStateChanged(Order orderState) {
		// TODO Auto-generated method stub
		
		if (_orderStateListeners == null || _orderStateListeners.isEmpty()) return;		
        
        Enumeration<iOrderStateEvent> e = _orderStateListeners.elements();
        while (e.hasMoreElements())
        {
        	iOrderStateEvent iOrderStateEvent = (iOrderStateEvent)e.nextElement();
        	iOrderStateEvent.OrderStateChanged(orderState);
        }
	}

	@Override
	public int ConnectionId() {
		// TODO Auto-generated method stub
		return this.connectionId;
	}

	@Override
	public String getConnectionString() {
		// TODO Auto-generated method stub
		return connectionString;
	}

	@Override
	public void setConnectionString(String connectionString) {
		// TODO Auto-generated method stub
		this.connectionString = connectionString;
		
	}

	
	@Override
	public String getExchangeCode() {
		return exchangeCode;
	}
	
	@Override
	public void setExchangeCode(String exchangeCode) {
		this.exchangeCode = exchangeCode;
	}

	@Override
	public void AddOrder(Order order) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void KillOrder(Order copy) {
		// TODO Auto-generated method stub
		
	}
	
}
