package net.bitfinex;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import javax.net.ssl.HttpsURLConnection;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import net.connector.FloodControl;
import net.connector.MarketData;
import net.connector.iMarketDataEvent;


public class MarketDataListener implements Runnable {
	iMarketDataEvent iMarketDataEvent;	
	FloodControl floodControl;
	int connectionId;
	ArrayList<String> tickersURLList = new ArrayList<String>();
	ArrayList<String> tickersList = new ArrayList<String>();
	HashMap<String, MarketData> marketDataHashMap = new HashMap<String, MarketData>();
	
	
	public MarketDataListener(iMarketDataEvent iMarketDataEvent, FloodControl floodControl, int connectionId){
		this.iMarketDataEvent = iMarketDataEvent;
		this.floodControl = floodControl;
		this.connectionId = connectionId;
	}
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		int tickersURLListPosition = 0;		
		ArrayList<URL> urlList = new ArrayList<URL>();		
		ObjectMapper mapper = new ObjectMapper();
		
		
		try {
			SetTradingPairsList();
			
			for (int it = 0; it < tickersURLList.size(); it++) {
				urlList.add(new URL(tickersURLList.get(it)));				
			}
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		Random rnd = new Random();
		
		
		while(true){
			
			long mills = Math.abs(rnd.nextLong()) % 100; 
			
			try {
				Thread.sleep(mills);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
			
			synchronized (this.floodControl) {
				int availableRequests = this.floodControl.AvailableRequests();
				
				if(availableRequests > 0){
					String jsonString = null;
					
					
					try {
						URL url = urlList.get(tickersURLListPosition);
						
						HttpsURLConnection httpsURLConnection = (HttpsURLConnection)url.openConnection();
						
						BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpsURLConnection.getInputStream()));
						jsonString = bufferedReader.readLine();
						
						JsonNode jn = mapper.readTree(jsonString);
						BitfinexTicker ticker = mapper.convertValue(jn, BitfinexTicker.class);
						
						
						MarketData marketData = marketDataHashMap.get(tickersList.get(tickersURLListPosition));
						if(marketData.getHighestBid() != ticker.getBid() || marketData.getLowestAsk() != ticker.getAsk()) {
							marketData.setLowestAsk(ticker.getAsk());
							marketData.setHighestBid(ticker.getBid());

							this.iMarketDataEvent.MarketDataChanged(this.connectionId, marketData);
						}
						
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					
					tickersURLListPosition = (tickersURLListPosition + 1)% tickersURLList.size();					
					this.floodControl.NewRequest();
				}
			}
		}
	}

	private void SetTradingPairsList() throws IOException {
		String gdax_ticker_url =  "https://api.bitfinex.com/v1/symbols";
		URL url = new URL(gdax_ticker_url);
		ObjectMapper mapper = new ObjectMapper();
		String tradingPairs = "";

		synchronized (this.floodControl) {
			int availableRequests = this.floodControl.AvailableRequests();
			
			if(availableRequests > 0){
				String jsonString = null;
				
				
				HttpsURLConnection httpsURLConnection = (HttpsURLConnection)url.openConnection();						
				
				BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpsURLConnection.getInputStream()));
				jsonString = bufferedReader.readLine();
				
				JsonNode jn = mapper.readTree(jsonString);
				
				TypeReference<ArrayList<String>> tr = new TypeReference<ArrayList<String>>() { };
				ArrayList<String> td = mapper.convertValue(jn, tr);
				
				for (String ticker : td) {
					tickersURLList.add("https://api.bitfinex.com/v1/pubticker/" + ticker);
					tickersList.add(ticker);
					
					MarketData marketData = new MarketData();
					marketData.setTrdPair(ticker);
					marketDataHashMap.put(ticker, marketData);
				}
				
				this.floodControl.NewRequest();
			}
		}
		
		if(tradingPairs.length() > 0) {
			tradingPairs = "?pair=" + tradingPairs.substring(1);
		}
		
		
	}
}

