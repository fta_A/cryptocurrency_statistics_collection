package net.codejava;

import java.util.HashMap;

public class Portfolio {
	int portfolio_id;
	//struct quotes_tbl_t *quotes_tbl;
	HashMap<String, Quote> quotes_tbl = new HashMap<String, Quote>();

	double wbid;
	double wask;

	double buy_at;
	double sell_at;

	double trd_amt;
	double buy_limit;
	double sell_limit;
	double shift;

	
	int active_orders;
	double amount;

	int work_type;
	char state;

	int zero_wbids_num;
	int zero_wasks_num;

	int spread;
	int mkt_spread;

	char trade;
	char buy;
	char sell;
}
