package net.codejava;


import java.lang.reflect.Constructor;
import java.lang.reflect.InvocationTargetException;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.HashMap;
import java.util.Map.Entry;

import net.connector.ConnectionState;
import net.connector.MarketData;
import net.connector.iConnectionEvent;
import net.connector.iExchangeConnector;
import net.connector.iMarketDataEvent;

public class DBSynchronizer  implements iConnectionEvent, iMarketDataEvent {
	HashMap<String, String> exchangesConnectionStrings;
	HashMap<Integer, iExchangeConnector> exchangeConnectors;
	
	//String JDBC_DRIVER = "com.mysql.cj.jdbc.Driver";
	//String DB_URL = "jdbc:mysql://151.80.45.53:32768/qa";
	//String USER = "root";

	
	String JDBC_DRIVER = "com.microsoft.sqlserver.jdbc.SQLServerDriver";	
	String DB_URL = "jdbc:sqlserver://151.80.45.53:1401;databaseName=qa";
	String USER = "SA";
	String PASS = "qFB4E97XpKHPxa";
	
	Connection conn = null;
	PreparedStatement preparedStmt = null;
	PreparedStatement truncateTmpTable = null;
	String dbInsertQuery;
	
	Long lastTmpTableClearing;
	
	public DBSynchronizer() {
		// TODO Auto-generated constructor stub
		
		exchangesConnectionStrings = new HashMap<String, String>();
		
		///*
		exchangesConnectionStrings.put("net.bitfinex.BitfinexConnector", "FLOOD_PERIOD=3;FLOOD_MSG=2;EXC_CODE=bitfinex");
		exchangesConnectionStrings.put("net.bitflyer.BitflyerConnector", "FLOOD_PERIOD=1;FLOOD_MSG=5;EXC_CODE=bitflyer");
		exchangesConnectionStrings.put("net.coinone.CoinoneConnector", "FLOOD_PERIOD=3;FLOOD_MSG=2;EXC_CODE=coinone");
		exchangesConnectionStrings.put("net.gdax.GdaxConnector", "FLOOD_PERIOD=1;FLOOD_MSG=3;EXC_CODE=gdax");
		exchangesConnectionStrings.put("net.korbit.KorbitConnector", "FLOOD_PERIOD=1;FLOOD_MSG=1;EXC_CODE=korbit");
		exchangesConnectionStrings.put("net.kraken.KrakenConnector", "FLOOD_PERIOD=2;FLOOD_MSG=1;EXC_CODE=kraken");
		exchangesConnectionStrings.put("net.poloniex.PoloniexConnector", "FLOOD_PERIOD=1;FLOOD_MSG=5;EXC_CODE=poloniex");		
		exchangesConnectionStrings.put("net.okcoin.OkcoinConnector", "FLOOD_PERIOD=1;FLOOD_MSG=2;EXC_CODE=okcoin");
		exchangesConnectionStrings.put("net.okcoincn.OkcoinCnConnector", "FLOOD_PERIOD=1;FLOOD_MSG=2;EXC_CODE=okcoin.cn");
		//*/
		
		
	
		//?????
		
		//
		
		exchangeConnectors = new HashMap<Integer, iExchangeConnector>();

		int exchangeId = 0;
		for(Entry<String, String> entry : exchangesConnectionStrings.entrySet()){
			
			
			try {
				Class<?> cl = Class.forName(entry.getKey());
				Constructor<?> con = cl.getConstructor(int.class);
				
				iExchangeConnector iExchangeConnector = (iExchangeConnector) con.newInstance(exchangeId);
				iExchangeConnector.setConnectionString(entry.getValue());
				
				iExchangeConnector.AddMarketDataListener(this);
				
				exchangeConnectors.put(exchangeId, iExchangeConnector);
				
				exchangeId++;
			} catch (InstantiationException | IllegalAccessException | ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (NoSuchMethodException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (SecurityException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (InvocationTargetException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
		}
		
		dbInsertQuery = "insert into ticker_data_tmp (ticker_timestamp, exchange_code, ticker_code, highest_bid, lowest_ask)"
				+ " values (?, ?, ?, ?, ?)";
		
		try {
			Class.forName(JDBC_DRIVER);
			conn = DriverManager.getConnection(DB_URL, USER, PASS);
			preparedStmt = conn.prepareStatement(dbInsertQuery);
			truncateTmpTable = conn.prepareStatement("truncate table ticker_data_tmp");
		} catch (ClassNotFoundException | SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		

		
			
		lastTmpTableClearing = System.currentTimeMillis();
	}
	
	public void Connect(){
		for (HashMap.Entry<Integer, iExchangeConnector> entry : exchangeConnectors.entrySet()) {
			entry.getValue().Connect();
		}

	}
	
	@Override
	public void ConnectionStateChanged(ConnectionState connectionState) {
		// TODO Auto-generated method stub
		System.out.println("Connection state changed: " + connectionState.stateDescription);
	}

	@Override
	public void MarketDataChanged(int connectionId, MarketData marketData) {
		// TODO Auto-generated method stub

		synchronized (this) {
			String momentStr = ZonedDateTime.now(ZoneOffset.UTC).toString();
			
			try {
				preparedStmt.setString (1, momentStr.substring(0, momentStr.length()-1));		
				preparedStmt.setString (2, exchangeConnectors.get(connectionId).getExchangeCode());
				preparedStmt.setString (3, marketData.getTrdPair());
				preparedStmt.setDouble (4, marketData.getHighestBid());
				preparedStmt.setDouble (5, marketData.getLowestAsk());
				
				preparedStmt.execute();	
				
				/*
				if(System.currentTimeMillis() - lastTmpTableClearing - 60 * 1000 >= 0) {
					lastTmpTableClearing = System.currentTimeMillis();
					truncateTmpTable.execute();
				}
				*/
			
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
			
		}
		
		//System.out.println("Moment: " + LocalDateTime.now() + ". Exchange: " + exchangeConnectors.get(connectionId).getExchangeCode() + ". Data: " + marketData.toString());	
		//System.out.println("Moment: " + ZonedDateTime.now(ZoneOffset.UTC) + ". Exchange: " + exchangeConnectors.get(connectionId).getExchangeCode() + ". Data: " + marketData.toString());	

	}
}
