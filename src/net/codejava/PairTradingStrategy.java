package net.codejava;


import java.util.ArrayList;
import java.util.HashMap;
import java.util.Random;

import net.connector.MarketData;
import net.connector.Order;
import net.connector.Order.MarketType;
import net.connector.Order.OrderDirection;
import net.connector.Order.OrderState;
import net.kraken.KrakenConnector;
import net.connector.iExchangeConnector;
import net.connector.iMarketDataEvent;
import net.connector.iOrderStateEvent;
import net.poloniex.PoloniexConnector;

public class PairTradingStrategy implements iOrderStateEvent, iMarketDataEvent {
	HashMap<Integer, Order> activeOrdersMap = new HashMap<Integer, Order>();
	HashMap<Integer, iExchangeConnector> exchangeConnectors = new HashMap<Integer, iExchangeConnector>();
	
	Portfolio portfolio = new Portfolio();
	
	ArrayList<Quote> quotesList = new ArrayList<Quote>();
	
	int orderId = 0;
	boolean only_first_time = false;
	
	public void SimulateTrading() {
		PoloniexConnector poloniexConnector = new PoloniexConnector(0);
		poloniexConnector.setConnectionString("FLOOD_PERIOD=2;FLOOD_MSG=1;EXC_CODE=poloniex");	
		poloniexConnector.AddOrderStateListener(this);
		poloniexConnector.AddMarketDataListener(this);	
		poloniexConnector.Subscribe("USDT_BTC");		
		exchangeConnectors.put(0, poloniexConnector);
		
		
		KrakenConnector krakenConnector = new KrakenConnector(1);
		krakenConnector.setConnectionString("FLOOD_PERIOD=2;FLOOD_MSG=1;EXC_CODE=kraken");	
		krakenConnector.AddOrderStateListener(this);
		krakenConnector.AddMarketDataListener(this);	
		krakenConnector.Subscribe("XXBTZUSD");		
		exchangeConnectors.put(1, krakenConnector);
		
		
		

		Quote quote = new Quote();
		quote.portfolio = portfolio;
		quote.weight = 1;
		quote.exchangeConnector = poloniexConnector;
		quote.ticker_code = "USDT_BTC";
		portfolio.quotes_tbl.put("0:" + quote.ticker_code, quote);
		quotesList.add(quote);
		
		quote = new Quote();
		quote.portfolio = portfolio;
		quote.weight = -1;
		quote.ticker_code = "XXBTZUSD";
		quote.exchangeConnector = krakenConnector;
		portfolio.quotes_tbl.put("1:" + quote.ticker_code, quote);
		quotesList.add(quote);
		
		
		portfolio.trade = 1;
		portfolio.buy = 1;
		portfolio.buy_at = -50;
		portfolio.sell_at = -10;
		portfolio.sell = 1;
		portfolio.trd_amt = 0.002;
		portfolio.buy_limit = portfolio.trd_amt * 12;
		portfolio.sell_limit = -portfolio.trd_amt * 12;
		portfolio.zero_wasks_num = 2;
		portfolio.zero_wbids_num = 2;
		portfolio.shift = 2;
		portfolio.amount = 0.008;
		
		poloniexConnector.Connect();
		krakenConnector.Connect();
		
	}


	@Override
	public void OrderStateChanged(Order orderState) {
		// TODO Auto-generated method stub
		
		System.out.println("OrderStateEvent: OrderId = " + orderState.userref 
				+ ", State = " + orderState.orderState 
				+ ", Amount = " + orderState.amount
				+ ", txid = " + (orderState.transIdList.size() > 0 ? orderState.transIdList.get(0) : "NA")
				);
		
		synchronized (activeOrdersMap) {
			if(orderState.orderState == OrderState.Active) {
				
				System.out.println("Added: OrderId = " + orderState.userref 
						+ ", State = " + orderState.orderState 
						+ ", Amount = " + orderState.amount
						+ ", txid = " + orderState.transIdList.get(0)
						);
			
				Order myOrder = this.activeOrdersMap.get(orderState.userref);
				myOrder.orderState = OrderState.Moving;
				
				
				if(orderState.amount == 0) {
					portfolio.active_orders--;
				}else {
					orderState.orderState = OrderState.Killing;
					orderState.exchangeConnector.KillOrder(orderState);
				}
			}else if(orderState.orderState == OrderState.Killed) {
				System.out.println("Killed: OrderId = " + orderState.userref 
						+ ", State = " + orderState.orderState 
						+ ", Amount = " + orderState.amount
						+ ", txid = " + orderState.transIdList.get(0)
						);
				
				Order myOrder = this.activeOrdersMap.get(orderState.userref);
				myOrder.orderState = OrderState.Moving;

				if(orderState.amount == 0) {
					portfolio.active_orders--;
					this.activeOrdersMap.remove(orderState.userref);
				}else if(myOrder.orderState == OrderState.Moving){
					
					if(orderState.dir == OrderDirection.buy)orderState.price = myOrder.quote.bid_tgt;
					else orderState.price = myOrder.quote.ask_tgt;
					
					orderState.orderState = OrderState.Adding;
					orderState.exchangeConnector.AddOrder(orderState);
				}	
			}else if(orderState.orderState == OrderState.Executed) {
				System.out.println("Executed: OrderId = " + orderState.userref 
						+ ", State = " + orderState.orderState 
						+ ", Amount = " + orderState.amount
						+ ", txid = " + orderState.transIdList.get(0)
						);
				
				portfolio.active_orders--;
				this.activeOrdersMap.remove(orderState.userref);
				
			}else if(orderState.orderState == OrderState.Refused) {

				System.out.println("Refused: OrderId = " + orderState.userref 
						+ ", State = " + orderState.orderState 
						+ ", Amount = " + orderState.amount
						+ ", Error = " + orderState.errorState
					//	+ ", txid = " + orderState.transIdList.get(0)
						);

				portfolio.trade = 0;
				portfolio.active_orders--;
				this.activeOrdersMap.remove(orderState.userref);
			
			}
	
		}
	}
	
	public void OrderStateChanged_2(Order orderState) {
		// TODO Auto-generated method stub
		
		
		synchronized (activeOrdersMap) {
			if(orderState.orderState == OrderState.Killed) {
				this.activeOrdersMap.remove(orderState.userref);				
				System.out.println("OrderId = " + orderState.userref + ", State = " + orderState.orderState + ", txid = " + orderState.transIdList.get(0));
			}else if(orderState.orderState == OrderState.Refused) {
				this.activeOrdersMap.remove(orderState.userref);
				System.out.println("OrderId = " + orderState.userref + ", State = " + orderState.orderState + ", Error = " + orderState.errorState);
			}else {
				Order myOrder = this.activeOrdersMap.get(orderState.userref);
				myOrder.orderState = orderState.orderState;
				
				if(orderState.transIdList == null) {
					System.out.println("orderState.transIdList == null");
				}
				
				System.out.println("OrderId = " + orderState.userref + ", State = " + orderState.orderState + ", txid = " + orderState.transIdList.get(0)); 
			}
		}
	}


	@Override
	public void MarketDataChanged(int connectionId, MarketData marketData) {
		// TODO Auto-generated method stub
		String key = connectionId + ":" + marketData.getTrdPair();
	
		Quote quote = portfolio.quotes_tbl.get(key);
		QuoteChanged(quote, marketData);
		
		/*
		System.out.println("Moment: " + ZonedDateTime.now(ZoneOffset.UTC) 
			+ ". Portfolio"
			+ ". Bid: " + portfolio.wbid
			+ ". ZBids: " + portfolio.zero_wbids_num
			+ ". Ask: " + portfolio.wask
			+ ". ZAsks: " + portfolio.zero_wasks_num
			);
		*/
		CheckPortfolio();
			
	}
	
	private void CheckPortfolio() {
		if(portfolio.trade == 0 || portfolio.active_orders > 0) return;
		
		if(portfolio.buy == 1 
			&& portfolio.zero_wasks_num == 0
			&& portfolio.buy_at > portfolio.wask
			&& portfolio.amount < portfolio.buy_limit
			) {			
			
			System.out.println("New Portfolio Trade: Buy, " + portfolio.wask);
			
			portfolio.amount += portfolio.trd_amt;
			portfolio.buy_at -= portfolio.shift;
			portfolio.sell_at -= portfolio.shift;
			
			for(Quote quote : portfolio.quotes_tbl.values()) {
				portfolio.active_orders++;
				
				Order order = new Order();
				order.trdPair = quote.ticker_code;
				order.dir = quote.weight > 0 ? OrderDirection.buy : OrderDirection.sell;
				order.amount =	portfolio.trd_amt;
				order.price = quote.weight > 0 ? quote.ask_tgt : quote.bid_tgt;
				order.marketType = MarketType.limit;
				order.userref = orderId++;
				order.orderState = OrderState.Adding;
				order.exchangeConnector = quote.exchangeConnector;
				order.quote = quote;
				
				synchronized (activeOrdersMap) {
					activeOrdersMap.put(order.userref, order);
				}			
				
				quote.exchangeConnector.AddOrder(order.getCopy());
				
				System.out.println("---New Trade: OrderId = " + order.userref 
						+ ", Pair = " + order.trdPair
						+ ", Amount = " + order.amount
						+ ", Price = " + order.price
						+ ", Dir = " + order.dir
						);
				
			}
			
		}else if(portfolio.sell == 1 
				&& portfolio.zero_wbids_num == 0
				&& portfolio.sell_at < portfolio.wbid
				&& portfolio.amount > portfolio.sell_limit
				) {
			
			System.out.println("New Portfolio Trade: Sell, " + portfolio.wbid);
			
			portfolio.amount -= portfolio.trd_amt;
			portfolio.buy_at += portfolio.shift;
			portfolio.sell_at += portfolio.shift;
			
			
			for(Quote quote : portfolio.quotes_tbl.values()) {
				portfolio.active_orders++;
				
				Order order = new Order();
				order.trdPair = quote.ticker_code;
				order.dir = quote.weight > 0 ? OrderDirection.sell : OrderDirection.buy;
				order.amount =	portfolio.trd_amt;
				order.price = quote.weight > 0 ? quote.bid_tgt : quote.ask_tgt;
				order.marketType = MarketType.limit;
				order.userref = orderId++;
				order.orderState = OrderState.Adding;
				order.exchangeConnector = quote.exchangeConnector;
				order.quote = quote;
				
				
				
				synchronized (activeOrdersMap) {
					activeOrdersMap.put(order.userref, order);
				}			
				
				
				if(only_first_time && quote.ticker_code == "XXBTZUSD") {
					//only_first_time = false;
					
					Order orderToSend = order.getCopy();					
					orderToSend.trdPair = "XXBTZEUR";
					//orderToSend.marketType = MarketType.market;
					if(orderToSend.dir == OrderDirection.buy)orderToSend.price = 4000;
					else orderToSend.price = 2000;
					
					quote.exchangeConnector.AddOrder(orderToSend);
					
					System.out.println("---New Trade: OrderId = " + orderToSend.userref 
							+ ", Pair = " + orderToSend.trdPair
							+ ", Amount = " + orderToSend.amount
							+ ", Price = " + orderToSend.price
							+ ", Dir = " + orderToSend.dir
							);
					
					if(portfolio.amount < portfolio.sell_limit) only_first_time = false;
					
				}else {
					quote.exchangeConnector.AddOrder(order.getCopy());
					
					System.out.println("---New Trade: OrderId = " + order.userref 
							+ ", Pair = " + order.trdPair
							+ ", Amount = " + order.amount
							+ ", Price = " + order.price
							+ ", Dir = " + order.dir
							);
				}
			}
		}
			
	}
	
	
	
	private void QuoteChanged(Quote quote, MarketData marketData) {
		double w_bid_ch = 0;
		double w_ask_ch = 0;

		
	
		

		if(quote.weight < 0){
			w_bid_ch = (marketData.getLowestAsk()  - quote.ask_tgt) * quote.weight;
			w_ask_ch = (marketData.getHighestBid()  - quote.bid_tgt) * quote.weight;
			
			
			if(quote.wask == 0 && w_ask_ch != 0)portfolio.zero_wbids_num--;
			else if(quote.wask != 0 && w_ask_ch == -quote.wask)portfolio.zero_wbids_num++;

			if(quote.wbid == 0 && w_bid_ch != 0)portfolio.zero_wasks_num--;
			else if(quote.wbid != 0 && w_bid_ch == -quote.wbid)portfolio.zero_wasks_num++;
		}else{
			w_bid_ch = (marketData.getHighestBid()  - quote.bid_tgt) * quote.weight;
			w_ask_ch = (marketData.getLowestAsk()  - quote.ask_tgt) * quote.weight;

			
			
			if(quote.wask == 0 && w_ask_ch != 0)portfolio.zero_wasks_num--;
			else if(quote.wask != 0 && w_ask_ch == -quote.wask)portfolio.zero_wasks_num++;

			if(quote.wbid == 0 && w_bid_ch != 0)portfolio.zero_wbids_num--;
			else if(quote.wbid != 0 && w_bid_ch == -quote.wbid)portfolio.zero_wbids_num++;
		}
		quote.wbid += w_bid_ch;
		quote.wask += w_ask_ch;
		
		
		portfolio.wbid += w_bid_ch;
		portfolio.wask += w_ask_ch;


		quote.bid_tgt = marketData.getHighestBid();
		quote.ask_tgt = marketData.getLowestAsk() ;
	}
	
	

	public void SimulateTrading_old() {
		PoloniexConnector poloniexConnector = new PoloniexConnector(0);
		poloniexConnector.setConnectionString("FLOOD_PERIOD=2;FLOOD_MSG=1;EXC_CODE=poloniex");	
		poloniexConnector.AddOrderStateListener(this);
		poloniexConnector.AddMarketDataListener(this);	
		poloniexConnector.Subscribe("USDT_BTC");		
		exchangeConnectors.put(0, poloniexConnector);
		
		
		KrakenConnector krakenConnector = new KrakenConnector(1);
		krakenConnector.setConnectionString("FLOOD_PERIOD=2;FLOOD_MSG=1;EXC_CODE=kraken");	
		krakenConnector.AddOrderStateListener(this);
		krakenConnector.AddMarketDataListener(this);	
		krakenConnector.Subscribe("XXBTZUSD");		
		exchangeConnectors.put(1, krakenConnector);
		
		
		

		Quote quote = new Quote();
		quote.portfolio = portfolio;
		quote.weight = 1;
		quote.exchangeConnector = poloniexConnector;
		quote.ticker_code = "USDT_BTC";
		portfolio.quotes_tbl.put("0:" + quote.ticker_code, quote);
		quotesList.add(quote);
		
		quote = new Quote();
		quote.portfolio = portfolio;
		quote.weight = -1;
		quote.ticker_code = "XXBTZUSD";
		quote.exchangeConnector = krakenConnector;
		portfolio.quotes_tbl.put("1:" + quote.ticker_code, quote);
		quotesList.add(quote);
		
		
		portfolio.trade = 1;
		portfolio.buy = 1;
		portfolio.buy_at = -30;
		portfolio.sell_at = 10;
		portfolio.sell = 0;
		portfolio.trd_amt = 0.002;
		portfolio.buy_limit = portfolio.trd_amt;
		portfolio.sell_limit = -portfolio.trd_amt;
		portfolio.zero_wasks_num = 2;
		portfolio.zero_wbids_num = 2;
		
		poloniexConnector.Connect();
		krakenConnector.Connect();
		
		
		int maxOrders = 2;
		//int orderId = 0;
		
		Random rnd = new Random(System.currentTimeMillis());
		
		while(true) {
			
			///*
			
			if(this.activeOrdersMap.size() < maxOrders && rnd.nextInt(10) < 5) {
			
				
				Order.OrderDirection dir = OrderDirection.buy;
				double price = 3000 - rnd.nextInt(500);
				
				if(rnd.nextInt(10) < 5) { 
					dir = OrderDirection.sell;
					price = 5000 + rnd.nextInt(500);
				}
				
				int rnd_quote = rnd.nextInt(10) % 2;
				quote = quotesList.get(rnd_quote);
				
				
				
				Order order = new Order();
				order.trdPair = quote.ticker_code;
				order.dir = dir;
				order.amount = 0.002;
				order.price = price;
				order.marketType = MarketType.limit;
				order.userref = orderId;
				order.orderState = OrderState.Adding;
				order.exchangeConnector = quote.exchangeConnector;
				
				synchronized (activeOrdersMap) {
					activeOrdersMap.put(order.userref, order);
				}
				
				
				quote.exchangeConnector.AddOrder(order.getCopy());
				
				orderId++;
			}
			
			synchronized (activeOrdersMap) {
				for(Order order : activeOrdersMap.values()) {
					if(rnd.nextInt(10) < 5 && order.orderState == OrderState.Active) {
						order.orderState = OrderState.Killing;
						order.exchangeConnector.KillOrder(order.getCopy());
					}
				}
			}
			//*/
			
			long mills = Math.abs(rnd.nextLong()) % 100; 
			
			//mills = 60000;
			
			try {
				Thread.sleep(mills);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
		
		//System.out.println("Bye!");
	}


}
