package net.codejava;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.security.MessageDigest;
import java.util.Base64;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.net.ssl.HttpsURLConnection;

import org.apache.commons.codec.binary.Hex;

public class PoloniexExchange {
	private final String URLString;
	private final String tradeBalancePathString;
	private final String openOrdersPathString;
	private final String closedOrdersPathString;
	private final String buyOrderPathString;
	private final String sellOrderPathString;
	private final String cancelOrderPathString;
	private final String apiKey;
	private final String privateKey;
	
	private static PoloniexExchange krExchange;
	
	PoloniexExchange() {
		URLString = "https://poloniex.com/tradingApi";
		tradeBalancePathString = "";
		openOrdersPathString = "";
		closedOrdersPathString = "";
		buyOrderPathString = "";
		sellOrderPathString = "";
		cancelOrderPathString = "";
		
		this.apiKey = "6TPTK5NS-TTTQZMF8-9DTRI5GU-SDORAM96";
		this.privateKey = "b4d962b2e88aba010876bcf2496efed76103f4ec0a351bd729a52b533984a2deb9bb51b7738d5fc57abee9fc9d50b1e426a86424c5a84870829f004084fff78c";
	}
	
	public static PoloniexExchange initialize() {
		if (krExchange == null) {
			krExchange = new PoloniexExchange();
			return krExchange;
		}
		else {
			return krExchange;
		}
	}
	
	public void getTradeBalance() {
		try {
			String nonce = String.valueOf(System.currentTimeMillis());
			String postData = "nonce=" + nonce;
			HttpsURLConnection httpsURLConnection = this.preparePostHeader(this.tradeBalancePathString, nonce, postData);
			
			OutputStreamWriter writer = new OutputStreamWriter(httpsURLConnection.getOutputStream());
            writer.write(postData);
            writer.flush();
			
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpsURLConnection.getInputStream()));
			 for ( String jsonString; (jsonString = bufferedReader.readLine()) != null; ) {
                     System.out.println( jsonString );
             }	 
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void getOpenOrders() {
		try {
			String nonce = String.valueOf(System.currentTimeMillis());
			String postData = "nonce=" + nonce;
			postData += "&command=returnOpenOrders";
			postData += "&currencyPair=all";
			
			HttpsURLConnection httpsURLConnection = this.preparePostHeader(this.openOrdersPathString, nonce, postData);
			
			OutputStreamWriter writer = new OutputStreamWriter(httpsURLConnection.getOutputStream());
            writer.write(postData);
            writer.flush();
			
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpsURLConnection.getInputStream()));
			 for ( String jsonString; (jsonString = bufferedReader.readLine()) != null; ) {
                     System.out.println( jsonString );
             }	 
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	public void getClosedOrders() {
		try {
			String nonce = String.valueOf(System.currentTimeMillis());
			String postData = "nonce=" + nonce;
//			postData += "&start=OWWDKL-TMBZA-47A223";
//			postData += "&start=OTFXGM-NHY2A-DAF6XR";
			HttpsURLConnection httpsURLConnection = this.preparePostHeader(this.closedOrdersPathString, nonce, postData);
			
			OutputStreamWriter writer = new OutputStreamWriter(httpsURLConnection.getOutputStream());
            writer.write(postData);
            writer.flush();
			
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpsURLConnection.getInputStream()));
			 for ( String jsonString; (jsonString = bufferedReader.readLine()) != null; ) {
                     System.out.println( jsonString );
             }	 
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void killOrder(String userRef) {
		try {
			String nonce = String.valueOf(System.currentTimeMillis());
			String postData = "nonce=" + nonce;
			postData += "&command=cancelOrder";
			postData += "&orderNumber=" + userRef;
			
			HttpsURLConnection httpsURLConnection = this.preparePostHeader(this.cancelOrderPathString, nonce, postData);
			
			OutputStreamWriter writer = new OutputStreamWriter(httpsURLConnection.getOutputStream());
            writer.write(postData);
            writer.flush();
			
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpsURLConnection.getInputStream()));
			 for ( String jsonString; (jsonString = bufferedReader.readLine()) != null; ) {
                     System.out.println( jsonString );
                     

             }	 
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	
	public void addBuyOrder(String pair, double rate, double amount) {
		try {
			String nonce = String.valueOf(System.currentTimeMillis());
			String postData = "nonce=" + nonce;
			postData += "&currencyPair=" + pair;
			postData += "&rate=" + rate;
			postData += "&amount=" + amount;
			
			HttpsURLConnection httpsURLConnection = this.preparePostHeader(this.buyOrderPathString, nonce, postData);
			
			OutputStreamWriter writer = new OutputStreamWriter(httpsURLConnection.getOutputStream());
            writer.write(postData);
            writer.flush();
			
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpsURLConnection.getInputStream()));
			 for ( String jsonString; (jsonString = bufferedReader.readLine()) != null; ) {
                     System.out.println( jsonString );
                     

             }
             	 
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	public void addSellOrder(String pair, double rate, double amount) {
		try {
			String nonce = String.valueOf(System.currentTimeMillis());
			String postData = "nonce=" + nonce;
			postData += "&command=sell";			
			postData += "&currencyPair=" + pair;
			postData += "&rate=" + rate;
			postData += "&amount=" + amount;
			
			
			HttpsURLConnection httpsURLConnection = this.preparePostHeader(this.sellOrderPathString, nonce, postData);
			
			OutputStreamWriter writer = new OutputStreamWriter(httpsURLConnection.getOutputStream());
            writer.write(postData);
            writer.flush();
			
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpsURLConnection.getInputStream()));
			 for ( String jsonString; (jsonString = bufferedReader.readLine()) != null; ) {
                     System.out.println( jsonString );
                     

             }
             	 
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	private HttpsURLConnection preparePostHeader (String pathURLString, String nonce, String postData) throws IOException {
		String fullURLString = this.URLString + pathURLString;
		URL postURL = new URL(fullURLString);
		HttpsURLConnection httpsURLConnection = (HttpsURLConnection)postURL.openConnection();
		httpsURLConnection.setRequestMethod("POST");
		httpsURLConnection.setRequestProperty("Key", this.apiKey);
		
		String calculatedSignature = this.calculateSignature(pathURLString, nonce, postData);
		httpsURLConnection.setRequestProperty("Sign", calculatedSignature);
		httpsURLConnection.setDoOutput(true);
		return httpsURLConnection;
	}
			
	private String calculateSignature(String uriPath, String nonce, String postData) {
	    String signature = "";
	    try {
			Mac mac = Mac.getInstance("HmacSHA512");
			mac.init(new SecretKeySpec(this.privateKey.getBytes(), "HmacSHA512"));
			signature = new String(Hex.encodeHex((mac.doFinal(postData.getBytes()))));
	    	
	    	
	    	
	    } catch(Exception e) {}
	    return signature;		
	}

	
}
