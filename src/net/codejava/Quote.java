package net.codejava;

import net.connector.iExchangeConnector;

public class Quote {
	public int quote_id;
	public int portfolio_id;
	
	public String ticker_code;

	public int isin_id;

	public int weight;

	public double wbid;
	public double wask;

	public double ask_tgt;
	public double bid_tgt;

	public double pt_value;

	public double price_scale;
	public double ptv_scale;

	public double minstep;

	public char is_left;
	public char is_quanto;
	public char qnt_type;
	
	public Portfolio portfolio;
	public iExchangeConnector exchangeConnector;
	//public struct isin_data_tbl_t *isin_data;
}
