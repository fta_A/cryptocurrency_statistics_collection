package net.codejava;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.URL;
import java.security.MessageDigest;
import java.util.Base64;

import javax.crypto.Mac;
import javax.crypto.spec.SecretKeySpec;
import javax.net.ssl.HttpsURLConnection;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

public class KrakenExchange {
	private final String URLString;
	private final String tradeBalancePathString;
	private final String openOrdersPathString;
	private final String closedOrdersPathString;
	private final String addOrderPathString;
	private final String cancelOrderPathString;
	private final String apiKey;
	private final String privateKey;
	
	private static KrakenExchange krExchange;
	
	KrakenExchange() {
		URLString = "https://api.kraken.com";
		tradeBalancePathString = "/0/private/TradeBalance";
		openOrdersPathString = "/0/private/OpenOrders";
		closedOrdersPathString = "/0/private/ClosedOrders";
		addOrderPathString = "/0/private/AddOrder";
		cancelOrderPathString = "/0/private/CancelOrder";
		
		apiKey = "sX4fQcjBimSnG1hcTDTQIii8nqE1aufrF4/uXiLtS2VKNAjqNkrcplo3";
		privateKey = "8vri6kR878738scIPa8IqfBpbri7U4aMhFO7/xE2wtcQ5k9iQFV4eb6V8Tyfz4d4Ju7mCk1XBRvTinz827zoBg==";
	}
	
	public static KrakenExchange initialize() {
		if (krExchange == null) {
			krExchange = new KrakenExchange();
			return krExchange;
		}
		else {
			return krExchange;
		}
	}
	
	public void getTradeBalance() {
		try {
			String nonce = String.valueOf(System.currentTimeMillis());
			String postData = "nonce=" + nonce;
			HttpsURLConnection httpsURLConnection = this.preparePostHeader(this.tradeBalancePathString, nonce, postData);
			
			OutputStreamWriter writer = new OutputStreamWriter(httpsURLConnection.getOutputStream());
            writer.write(postData);
            writer.flush();
			
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpsURLConnection.getInputStream()));
			 for ( String jsonString; (jsonString = bufferedReader.readLine()) != null; ) {
                     System.out.println( jsonString );
             }	 
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void getOpenOrders() {
		try {
			String nonce = String.valueOf(System.currentTimeMillis());
			String postData = "nonce=" + nonce;
			HttpsURLConnection httpsURLConnection = this.preparePostHeader(this.openOrdersPathString, nonce, postData);
			
			OutputStreamWriter writer = new OutputStreamWriter(httpsURLConnection.getOutputStream());
            writer.write(postData);
            writer.flush();
			
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpsURLConnection.getInputStream()));
			 for ( String jsonString; (jsonString = bufferedReader.readLine()) != null; ) {
                     System.out.println( jsonString );
             }	 
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	public void getClosedOrders() {
		try {
			String nonce = String.valueOf(System.currentTimeMillis());
			String postData = "nonce=" + nonce;
//			postData += "&start=OWWDKL-TMBZA-47A223";
//			postData += "&start=OTFXGM-NHY2A-DAF6XR";
			HttpsURLConnection httpsURLConnection = this.preparePostHeader(this.closedOrdersPathString, nonce, postData);
			
			OutputStreamWriter writer = new OutputStreamWriter(httpsURLConnection.getOutputStream());
            writer.write(postData);
            writer.flush();
			
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpsURLConnection.getInputStream()));
			 for ( String jsonString; (jsonString = bufferedReader.readLine()) != null; ) {
                     System.out.println( jsonString );
             }	 
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	public void killOrder(int userRef) {
		try {
			String nonce = String.valueOf(System.currentTimeMillis());
			String postData = "nonce=" + nonce;
			postData += "&txid=" + userRef;
			
			HttpsURLConnection httpsURLConnection = this.preparePostHeader(this.cancelOrderPathString, nonce, postData);
			
			OutputStreamWriter writer = new OutputStreamWriter(httpsURLConnection.getOutputStream());
            writer.write(postData);
            writer.flush();
			
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpsURLConnection.getInputStream()));
			 for ( String jsonString; (jsonString = bufferedReader.readLine()) != null; ) {
                     System.out.println( jsonString );
                     

             }	 
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	
	public void addOrder(String pair, String type, String orderType, double price, double volume, int userRef) {
		try {
			String nonce = String.valueOf(System.currentTimeMillis());
			String postData = "nonce=" + nonce;
			postData += "&pair=" + pair;
			postData += "&type=" + type;
			postData += "&ordertype=" + orderType;
			postData += "&price=" + price;
			postData += "&volume=" + volume;
			postData += "&userref=" + userRef;
			
			HttpsURLConnection httpsURLConnection = this.preparePostHeader(this.addOrderPathString, nonce, postData);
			
			OutputStreamWriter writer = new OutputStreamWriter(httpsURLConnection.getOutputStream());
            writer.write(postData);
            writer.flush();
			
			BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(httpsURLConnection.getInputStream()));
			 for ( String jsonString; (jsonString = bufferedReader.readLine()) != null; ) {
                     System.out.println( jsonString );
                     

             }
             	 
		} catch (IOException e) {
			e.printStackTrace();
		}
		
	}
	
	private HttpsURLConnection preparePostHeader (String pathURLString, String nonce, String postData) throws IOException {
		String fullURLString = this.URLString + pathURLString;
		URL postURL = new URL(fullURLString);
		HttpsURLConnection httpsURLConnection = (HttpsURLConnection)postURL.openConnection();
		httpsURLConnection.setRequestMethod("POST");
		httpsURLConnection.setRequestProperty("API-Key", this.apiKey);
		
		String calculatedSignature = this.calculateSignature(pathURLString, nonce, postData);
		httpsURLConnection.setRequestProperty("API-Sign", calculatedSignature);
		httpsURLConnection.setDoOutput(true);
		return httpsURLConnection;
	}
			
	private String calculateSignature(String uriPath, String nonce, String postData) {
	    String signature = "";
	    try {
	        MessageDigest md = MessageDigest.getInstance("SHA-256");
	        md.update((nonce + postData).getBytes());
	        Mac mac = Mac.getInstance("HmacSHA512");
	        mac.init(new SecretKeySpec(Base64.getDecoder().decode(this.privateKey.getBytes()), "HmacSHA512"));
	        mac.update(uriPath.getBytes());
	        signature = new String(Base64.getEncoder().encode(mac.doFinal(md.digest())));
	    } catch(Exception e) {}
	    return signature;		
	}
}
